//
//  PAItemPageSecondViewController.swift
//  Parduota
//
//  Created by TungHN on 11/20/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import DropDown

class PAItemSecondPageViewController: PABaseViewController {

    @IBOutlet weak var nextPageButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var titlePageLabel: UILabel!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var quantityView: UIView!
    @IBOutlet weak var quantityTextField: UITextField!
    @IBOutlet weak var chooseConditionView: UIView!
    @IBOutlet weak var chooseConditionTextField: UITextField!
    @IBOutlet weak var showConditionButton: UIButton!
    @IBOutlet weak var chooseCountryView: UIView!
    @IBOutlet weak var chooseCountryTextField: UITextField!
    @IBOutlet weak var showCountryButton: UIButton!
    @IBOutlet weak var addressView: UIView!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var sellForCharityLabel: UILabel!
    @IBOutlet weak var sellOfCharitySwitch: UISwitch!
    @IBOutlet weak var showShippingTypeButton: UIButton!
    @IBOutlet weak var shippingTypeTextField: UITextField!
    
    var pageViewController: PAPageViewController?
    var newItem: PAItemModel!
    let chooseCondition = DropDown()
    let chooseCountry = DropDown()
    let chooseShippingType = DropDown()
    let listConditions = Array(PAAppConstants.CONDITION.values.sorted())
    let listCountries = Array(PAAppConstants.COUNTRY.values.sorted())
    let listShippingType = Array(PAAppConstants.LIST_SHIPPING_TYPE_CUSTOMER.values.sorted())
    
    override func viewDidLoad() {
        super.viewDidLoad()

        newItem = PAItemService.share.newItem!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func configView() {
        titlePageLabel.text = R.string.localizable.title_fill_item()
        sellForCharityLabel.text = R.string.localizable.title_sell_for_charity()
        
        _ = nextPageButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.nextPageAction()
                })
        }
        
        _ = previousButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.previousPageAction()
                })
        }
        
        titlePageLabel.text = NSLocalizedString("title_fill_item", comment: TSConstants.EMPTY_STRING)
        
        _ = priceTextField.then {
            $0.placeholder = R.string.localizable.title_price()
            $0.keyboardType = .numberPad
            _ = $0.rx.controlEvent(.allEditingEvents)
            .asObservable()
            .subscribe(onNext: { [weak self] in
                guard let price = self?.priceTextField.text else { return }
                self?.newItem.price = price
            })
        }
        
        _ = quantityTextField.then {
            $0.placeholder = R.string.localizable.title_quantity()
            $0.keyboardType = .numberPad
            _ = $0.rx.controlEvent(.allEditingEvents)
                .asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let quantity = self?.quantityTextField.text, quantity != TSConstants.EMPTY_STRING else { return }
                    self?.newItem.quantity = quantity
                })
        }
        
        _ = chooseConditionTextField.then {
            $0.placeholder = R.string.localizable.title_choose_condition()
            $0.rightViewMode = .always
            let imageView = UIImageView.init(image: R.image.icon_dropdown())
            imageView.frame = CGRect.init(x: 0, y: 0, width: 25, height: 25)
            $0.rightView = imageView
        }
        
        _ = showConditionButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.view.endEditing(true)
                    self?.actionChooseCondition()
                })
        }
        
        _ = chooseCountryTextField.then {
            $0.placeholder = R.string.localizable.title_choose_country()
            $0.rightViewMode = .always
            let imageView = UIImageView.init(image: R.image.icon_dropdown())
            imageView.frame = CGRect.init(x: 0, y: 0, width: 25, height: 25)
            $0.rightView = imageView
        }
        
        _ = showCountryButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.view.endEditing(true)
                    self?.actionChooseCountry()
                })
        }
        
        _ = addressTextField.then {
            $0.placeholder = R.string.localizable.title_address()
            _ = $0.rx.controlEvent(.allEditingEvents)
                .asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let address = self?.addressTextField.text else { return }
                    self?.newItem.location = address
                })
        }
        
        _ = sellOfCharitySwitch.then( {
            _ = $0.rx.controlEvent(.valueChanged)
                .asObservable()
                .subscribe(onNext: { [weak self] in
                    self?.newItem.sellForCharity = (self?.sellOfCharitySwitch.isOn.hashValue)!
                })
        })
        _ = shippingTypeTextField.then {
            $0.placeholder = R.string.localizable.shipping_type_freight()
            $0.rightViewMode = .always
            let imageView = UIImageView.init(image: R.image.icon_dropdown())
            imageView.frame = CGRect.init(x: 0, y: 0, width: 25, height: 25)
            $0.rightView = imageView
        }
        
        _ = showShippingTypeButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.view.endEditing(true)
                    self?.actionChooseShippingType()
                })
        }
        
        setupDropDown()
    }
}

// MARK: - Action

extension PAItemSecondPageViewController {
    func nextPageAction() {
        
        guard newItem.price != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input price!").subscribe()
            
            return
        }
        
        guard newItem.quantity != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input quantity!").subscribe()
            
            return
        }
        
        guard newItem.condition != 0 else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input condition!").subscribe()
            
            return
        }
        
        guard newItem.country != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input country!").subscribe()
            
            return
        }
        
        guard newItem.location != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input location!").subscribe()
            
            return
        }
        
        guard newItem.shippingTypeCustom != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input shipping type!").subscribe()
            
            return
        }
        
        pageViewController = self.parent as? PAPageViewController
        
        guard let pageViewController = pageViewController else {
            return
        }
        
        if let thirdVC = pageViewController.orderedViewControllers.last as? PAItemThirdPageViewController {
            pageViewController.setViewControllers([thirdVC],
                                                  direction: .forward,
                                                  animated: true,
                                                  completion: nil)
        }
    }
    
    func previousPageAction() {
        
        pageViewController = self.parent as? PAPageViewController
        
        guard let pageViewController = pageViewController else {
            return
        }
        
        if let firstVC = pageViewController.orderedViewControllers.first as? PAItemFirstPageViewController {
            pageViewController.setViewControllers([firstVC],
                                                  direction: .reverse,
                                                  animated: true,
                                                  completion: nil)
            
        }
    }
    
    func setupDropDown() {
        chooseCondition.anchorView = chooseConditionTextField
        chooseCondition.direction = .bottom
        chooseCondition.dataSource = listConditions
        chooseCondition.selectionAction = { [unowned self] (index, item) in
            self.chooseConditionTextField.text = item
            self.newItem.condition = PAAppConstants.CONDITION.allKeysForValue(val: item).first!
        }
        
        chooseCountry.anchorView = chooseConditionTextField
        chooseCountry.direction = .bottom
        chooseCountry.dataSource = listCountries
        chooseCountry.selectionAction = { [unowned self] (index, item) in
            self.chooseCountryTextField.text = item
            self.newItem.country = PAAppConstants.COUNTRY.allKeysForValue(val: item).first!
        }
        
        chooseShippingType.anchorView = chooseCountryTextField
        chooseShippingType.direction = .bottom
        chooseShippingType.dataSource = listShippingType
        chooseShippingType.selectionAction = { [unowned self] (index, item) in
            self.shippingTypeTextField.text = item
            self.newItem.shippingTypeCustom = PAAppConstants.LIST_SHIPPING_TYPE_CUSTOMER.allKeysForValue(val: item).first!
        }
    }
    
    func actionChooseCondition() {
        chooseCondition.show()
    }
    
    func actionChooseCountry() {
        chooseCountry.show()
    }
    
    func actionChooseShippingType() {
        chooseShippingType.show()
    }
}

//MARK: - UITextFieldDelegate
extension PAItemSecondPageViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {

        PAShareData.shareInstance.changeStateToBeginEditting(view: textField.superview)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToEndEditting(view: textField.superview)
    }
}

