//
//  PADetailItemViewController.swift
//  Parduota
//
//  Created by TungHN on 11/30/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import SDWebImage
import ImageSlideshow

class PADetailItemViewController: PABaseViewController {

    @IBOutlet weak var slideshow: ImageSlideshow!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    @IBOutlet weak var itemPriceLabel: UILabel!
    @IBOutlet weak var itemQuantityLabel: UILabel!
    @IBOutlet weak var itemDescriptionLabel: UILabel!
    @IBOutlet weak var itemLocationLabel: UILabel!
    @IBOutlet weak var itemConditionLabel: UILabel!
    @IBOutlet weak var itemShippingTypeLabel: UILabel!
    
    
    open var item: PAItemModel? = PAItemModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func configView() {
        var links = Array<SDWebImageSource>()
        if let mediaArr = item?.media {
            for media in mediaArr {
                links.append(SDWebImageSource(urlString: PAAppConstants.URL_BASE_IMAGE + media.link)!)
            }
        }
        
        _ = slideshow.then {
            $0.backgroundColor = UIColor.white
            $0.slideshowInterval = 5.0
            $0.pageControlPosition = PageControlPosition.underScrollView
            $0.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
            $0.pageControl.pageIndicatorTintColor = UIColor.black
            $0.contentScaleMode = UIViewContentMode.scaleAspectFill
            $0.activityIndicator = DefaultActivityIndicator()
            $0.currentPageChanged = { page in
                print("current page:", page)
            }
            $0.setImageInputs(links)
            $0.contentScaleMode = .scaleAspectFit
            let recognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
            $0.addGestureRecognizer(recognizer)
        }
        
        itemTitleLabel.text = item?.title
        if let created = item?.createdAt, created != TSConstants.EMPTY_STRING {
            createdLabel.text = String("at " + created)
        }
        
        if let price = item?.price, price != TSConstants.EMPTY_STRING {
            itemPriceLabel.text = String(R.string.localizable.title_price() + ": " + price)
        }
        
        if let quantity = item?.quantity, quantity != TSConstants.EMPTY_STRING {
            itemQuantityLabel.text = String(R.string.localizable.title_quantity() + ": " + String(quantity))
        }
        
        if let description = item?.descriptionField, description != TSConstants.EMPTY_STRING {
            itemDescriptionLabel.text = description
        }
        
        if let location = item?.location, location != TSConstants.EMPTY_STRING {
            itemLocationLabel.text = location
        }
        
        if let condition = item?.condition, condition != 0 {
            itemConditionLabel.text = PAAppConstants.CONDITION[condition]
        }
        
        if let shippingType = item?.shippingTypeCustom, shippingType != TSConstants.EMPTY_STRING {
            itemShippingTypeLabel.text = PAAppConstants.LIST_SHIPPING_TYPE_CUSTOMER[shippingType]
        }
    }
}

// MARK : Action
extension PADetailItemViewController {
    func didTap() {
        let fullScreenController = slideshow.presentFullScreenController(from: self)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
}
