//
//  PAItemPageThirdViewController.swift
//  Parduota
//
//  Created by TungHN on 11/20/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import Popover
import SDWebImage

class PAItemThirdPageViewController: PABaseViewController {

    @IBOutlet weak var titlePageLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var imageTitleLabel: UILabel!
    @IBOutlet weak var dimensionsView: UIView!
    @IBOutlet weak var dimensionsLabel: UILabel!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var lengthTextField: UITextField!
    @IBOutlet weak var widthTextField: UITextField!
    @IBOutlet weak var heightTextField: UITextField!
    @IBOutlet weak var saveItemButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    
    var pageViewController: PAPageViewController?
    var newItem: PAItemModel!
    var picker = UIImagePickerController()
    fileprivate let menuAddImage = [R.string.localizable.title_take_photo(), R.string.localizable.title_choose_from_library()]
    fileprivate var popover: Popover!
    fileprivate var mediaArr = [PAMediaModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        newItem = PAItemService.share.newItem!
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if newItem.shippingTypeCustom == "local_pickup" || newItem.shippingTypeCustom == "freight" {
            dimensionsView.isHidden = true
        } else {
            dimensionsView.isHidden = false
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func configView() {
        titlePageLabel.text = R.string.localizable.title_media()
        imageTitleLabel.text = TSConstants.WHITESPACE_STRING + R.string.localizable.title_image()
        dimensionsLabel.text = TSConstants.WHITESPACE_STRING + R.string.localizable.title_dimensions()
        picker.delegate = self
        
        _ = collectionView.then {
            $0.delegate = self
            $0.dataSource = self
        }
        
        _ = previousButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.previousPageAction()
                })
        }
        
        _ = weightTextField.then {
            $0.placeholder = R.string.localizable.title_weight()
            $0.keyboardType = .numberPad
            _ = $0.rx.controlEvent(.allEditingEvents)
                .asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let weight = self?.weightTextField.text else { return }
                    self?.newItem.weight = weight
                })
        }
        
        _ = widthTextField.then {
            $0.placeholder = R.string.localizable.title_width()
            $0.keyboardType = .numberPad
            _ = $0.rx.controlEvent(.allEditingEvents)
                .asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let width = self?.widthTextField.text else { return }
                    self?.newItem.width = width
                })
        }
        
        _ = lengthTextField.then {
            $0.placeholder = R.string.localizable.title_length()
            $0.keyboardType = .numberPad
            _ = $0.rx.controlEvent(.allEditingEvents)
                .asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let length = self?.lengthTextField.text else { return }
                    self?.newItem.length = length
                })
        }
        
        _ = heightTextField.then {
            $0.placeholder = R.string.localizable.title_height()
            $0.keyboardType = .numberPad
            _ = $0.rx.controlEvent(.allEditingEvents)
                .asObservable()
                .subscribe(onNext: { [weak self] in
                    guard let height = self?.heightTextField.text else { return }
                    self?.newItem.height = height
                })
        }
        
        _ = saveItemButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.saveNewItem()
                })
        }
    }
}

// MARK: - Action
extension PAItemThirdPageViewController {
    func previousPageAction() {
        
        pageViewController = self.parent as? PAPageViewController
        
        guard let pageViewController = pageViewController else {
            return
        }
        
        if let secondVC = pageViewController.orderedViewControllers[1] as? PAItemSecondPageViewController {
            pageViewController.setViewControllers([secondVC],
                                                  direction: .reverse,
                                                  animated: true,
                                                  completion: nil)
            
        }
    }
    
    func showPopoverToAddImageFromCell(cell: PAAddImageCollectionViewCell) {
        let tableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 90))
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isScrollEnabled = false
        self.popover = Popover(options: [.type(.down),
                                         .blackOverlayColor(UIColor(white: 0.0, alpha: 0.6))])
        
        self.popover.show(tableView, fromView: cell)
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerControllerSourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary() {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func saveNewItem() {
        var dataJson = newItem.convertToDictionary()
        var mediaID = String()
        for media in mediaArr {
            mediaID.append(String(media.id) + TSConstants.COMMA_STRING)
        }
        
        let mediaIDLast = mediaID.substring(to: mediaID.index(before: mediaID.endIndex))
        dataJson[PAAppConstants.MEDIA] = mediaIDLast
        PAItemService.share.addNewItem(item: dataJson, success: { [weak self] in
            _ = self?.rx_alertWithMessage(message: R.string.localizable.message_success(),
                                    cancelTitle: R.string.localizable.message_ok(),
                                    otherTitles: []).subscribe(onNext: { (_) in
                                        self?.navigationController?.popViewController(animated: true)
                                    })
        }, failure: nil)
        
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension PAItemThirdPageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if mediaArr.count == 12 {
            return 12
        }
        
        return mediaArr.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = TSBaseCollectionViewCell()
        
        if indexPath.row == mediaArr.count  && mediaArr.count < 12 {
            cell = addNewImageCellFor(collectionView: collectionView, atIndexPath: indexPath)
        } else {
            cell = itemImageCellFor(collectionView: collectionView, atIndexPath: indexPath, withMedia: mediaArr)
        }
        
        cell.setIndexPath(indexPath: indexPath, sender: self)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        if (cell?.isKind(of: PAAddImageCollectionViewCell.self))! {
            showPopoverToAddImageFromCell(cell: cell as! PAAddImageCollectionViewCell)
        } else if (cell?.isKind(of: PAItemImageCollectionViewCell.self))! {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 100, height: 100)
    }
}

// MARK: Custom cell
extension PAItemThirdPageViewController {
    func addNewImageCellFor(collectionView: UICollectionView, atIndexPath indexPath: IndexPath) -> PAAddImageCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.pAAddImageCollectionViewCell, for: indexPath)! as PAAddImageCollectionViewCell
        
        return cell
    }
    
    func itemImageCellFor(collectionView: UICollectionView, atIndexPath indexPath: IndexPath, withMedia mediaArr: Array<PAMediaModel>) -> PAItemImageCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: R.reuseIdentifier.pAItemImageCollectionViewCell, for: indexPath)! as PAItemImageCollectionViewCell
        cell.setIndexPath(indexPath: indexPath, sender: self)
        if indexPath.row < mediaArr.count {
            let media = mediaArr[indexPath.row]
            cell.configCellWithData(data: media)
        }
        
        return cell
    }
}

// MARK: - UITextFieldDelegate
extension PAItemThirdPageViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        PAShareData.shareInstance.changeStateToBeginEditting(view: textField.superview)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToEndEditting(view: textField.superview)
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension PAItemThirdPageViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return menuAddImage.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = menuAddImage[(indexPath as NSIndexPath).row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.popover.dismiss()
        
        switch indexPath.row {
        case 0:
            openCamera()
            
            break
        case 1:
            openGallary()
            
            break
        default:
            break
        }
    }
}

// MARK : - UIImagePickerControllerDelegate
extension PAItemThirdPageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if let dataImage = UIImagePNGRepresentation(pickedImage) {
                PAMediaService.share.uploadImage(image: dataImage, success: { [weak self] (object) in
                    if object.isKind(of: PAMediaModel.self) {
                        let media = object as! PAMediaModel
                        self?.mediaArr.append(media)
                        
                        self?.collectionView.reloadData()
                    }
                }, failure: nil)
            }
        }
        
        dismiss(animated: false, completion: nil)
    }
}

// MARK: - PAItemImageCollectionViewCellDelegate
extension PAItemThirdPageViewController: PAItemImageCollectionViewCellDelegate {
    func deleteImageInIndex(index: IndexPath) {
        if index.row < mediaArr.count {
            let media = mediaArr[index.row]
            PAMediaService.share.deleteImage(imageID: media.id, success: { [weak self] in
                self?.mediaArr.remove(at: index.row)
                self?.collectionView.reloadData()
            }, failure: nil)
        }
    }
}
