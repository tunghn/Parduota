//
//  PAItemPageOneViewController.swift
//  Parduota
//
//  Created by TungHN on 11/20/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAItemFirstPageViewController: PABaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleValueTextField: UITextField!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var descriptionValueUITextView: UITextView!
    @IBOutlet weak var nextPageButton: UIButton!
    @IBOutlet weak var titlePageLabel: UILabel!
    
    
    var pageViewController: PAPageViewController?
    var newItem: PAItemModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func configView() {
        let tap = UITapGestureRecognizer.init(target: self, action: #selector(endEditing))
        self.view.addGestureRecognizer(tap)
        
        _ = titlePageLabel.then {
            $0.text = NSLocalizedString("title_and_description", comment: TSConstants.EMPTY_STRING)
        }
        
        _ = titleLabel.then {
            $0.text = NSLocalizedString("title", comment: TSConstants.EMPTY_STRING)
        }
        
        _ = titleValueTextField.then {
            $0.placeholder = R.string.localizable.title()
            $0.text = newItem.title
        }
        
        _ = descriptionTitleLabel.then {
            $0.text = R.string.localizable.title_description()
        }
        
        _ = descriptionValueUITextView.then {
            $0.delegate = self
            $0.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.5).cgColor
            $0.layer.borderWidth = 0.5
            $0.text = newItem.descriptionField
        }
        
        _ = nextPageButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.nextPageAction()
                })
        }
    }
    
    func endEditing() {
        self.view.endEditing(true)
    }
}

//MARK: - ACtion
extension PAItemFirstPageViewController {
    func nextPageAction() {
        guard let title = titleValueTextField.text?.trim(), title != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input title!").subscribe()
            
            return
        }
        
        guard let descriptionValue = descriptionValueUITextView.text?.trim(), descriptionValue != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input description!").subscribe()
            
            return
        }
        
        pageViewController = self.parent as? PAPageViewController
        
        guard let pageViewController = pageViewController else {
            return
        }
        
        if let secondVC = pageViewController.orderedViewControllers[1] as? PAItemSecondPageViewController {
            newItem.title = title
            newItem.descriptionField = descriptionValue
            pageViewController.setViewControllers([secondVC],
                                                  direction: .forward,
                                                  animated: true,
                                                  completion: nil)
        }
    }
}

// MARK : - UITextViewDelegate
extension PAItemFirstPageViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        let newPosition = textView.endOfDocument
        textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
    }
}

