//
//  PAPageViewController.swift
//  Parduota
//
//  Created by TungHN on 11/20/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAPageViewController: UIPageViewController {
    
    private(set) lazy var orderedViewControllers: [PABaseViewController] = {
        return [R.storyboard.pageVC.pAItemPageOneViewController(),
                R.storyboard.pageVC.pAItemPageSecondViewController(),
                R.storyboard.pageVC.pAItemPageThirdViewController()]
        }() as! [PABaseViewController]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let firstVC = orderedViewControllers.first {
            if let itemFirstPage = firstVC as? PAItemFirstPageViewController {
                itemFirstPage.newItem = PAItemService.share.getNewItem()
                setViewControllers([itemFirstPage],
                                   direction: .forward,
                                   animated: true,
                                   completion: nil)
            }
        }
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

// MARK: UIPageViewControllerDataSource
/*
extension PAPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController as! PABaseViewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController as! PABaseViewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }

}
*/
