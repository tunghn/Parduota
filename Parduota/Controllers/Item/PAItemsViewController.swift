//
//  PAItemsViewController.swift
//  Parduota
//
//  Created by TungHN on 11/6/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAItemsViewController: PABaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addNewItemButton: UIButton!

    var currentPage = 1
    var loadingData = false
    var status = 0
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getItemsAtPage(page: currentPage, reloadData: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarItem()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: R.image.icon_notification(), style: .done, target: self, action: #selector(showNotification))
    }
    
    override func configView() {
        
        _ = tableView.then {
            $0.dataSource = self
            $0.delegate = self
            $0.separatorStyle = .none
        }
        
        _ = addNewItemButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.addNewItem()
                })
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(getData(notification:)), name: NSNotification.Name(rawValue: "IndexMenu"), object: nil)
    }
}

// MARK: - Action
extension PAItemsViewController {
    func getItemsAtPage(page: Int, reloadData: Bool) {
        tableView.isHidden = true
        PAItemService.share.getItemsListWithPage(page: page, isReloadData: reloadData, success: { [weak self] in
            self?.loadingData = PAItemService.share.isLoadMore()
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
        }, failure: nil)
    }
    
    func getItemsList(page: Int, status: Int, reloadData: Bool) {
        tableView.isHidden = true
        PAItemService.share.getItemsList(page: page, status: status, isReloadData: reloadData, success: { [weak self] in
            self?.loadingData = PAItemService.share.isLoadMore()
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
        }, failure: nil)
    }
    
    func addNewItem() {
        guard let pageVC = R.storyboard.pageVC.pAPageViewController() else {
            return
        }
        
        self.navigationController?.pushViewController(pageVC, animated: true)
    }
    
    func showNotification() {
        if let vc = R.storyboard.notification.pAListNotificationViewController() {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func getData(notification: Notification) {
        currentPage = 1
        guard let userInfo = notification.userInfo,
            let index  = userInfo["index"] as? Int else { return }
        self.index = index
        if index == 0 {
            self.getItemsAtPage(page: currentPage, reloadData: true)
        } else {
            self.getItemsListAtIndex(index: index)
        }
    }
    
    func getItemsListAtIndex(index: Int) {
        switch index {
        case 1:
            self.getItemsList(page: currentPage, status: Item_Status.Active.rawValue, reloadData: true)
            
            break
        case 2:
            self.getItemsList(page: currentPage, status: Item_Status.Sold.rawValue, reloadData: true)
            
            break
        case 3:
            self.getItemsList(page: currentPage, status: Item_Status.Pending.rawValue, reloadData: true)
            
            break
        case 4:
            self.getItemsList(page: currentPage, status: Item_Status.Draft.rawValue, reloadData: true)
            
            break
        case 5:
            self.getItemsList(page: currentPage, status: Item_Status.Reject.rawValue, reloadData: true)
            
            break
        default:
            break
        }
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension PAItemsViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PAItemService.share.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.pAItemTableViewCell, for: indexPath)! as PAItemTableViewCell
        guard let item = PAItemService.share.getItemAtIndexPath(indexPath: indexPath.row) else {
            return cell
        }
        
        cell.configCellWithData(data: item)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if loadingData && indexPath.row == PAItemService.share.numberOfItems() - 1 {
            currentPage+=1
            if self.index == 0 {
                self.getItemsAtPage(page: currentPage, reloadData: false)
            } else {
                self.getItemsListAtIndex(index: index)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = R.storyboard.main.pADetailItemViewController() {
            if let item = PAItemService.share.getItemAtIndexPath(indexPath: indexPath.row) {
                vc.item = item
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension PAItemsViewController: SlideMenuControllerDelegate {
    
}
