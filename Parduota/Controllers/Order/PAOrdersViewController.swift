//
//  PAOrdersViewController.swift
//  Parduota
//
//  Created by TungHN on 11/6/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAOrdersViewController: PABaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addOrderButton: UIButton!
    
    var currentPage = 1
    var loadingData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        self.setNavigationBarItem()
        currentPage = 1
        self.getOrdersAtPage(page: currentPage, reloadData: true)
    }
    
    override func configView() {
        _ = tableView.then {
            $0.dataSource = self
            $0.delegate = self
            $0.separatorStyle = .none
        }
        
        _ = addOrderButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.addNewOrder()
                })
        }
    }
}

// MARK: - Action
extension PAOrdersViewController {
    func getOrdersAtPage(page: Int, reloadData: Bool) {
        PAOrderService.share.getOrdersListAtPage(page: page, isReloadData: reloadData, success: { [weak self] in
            self?.loadingData = PAOrderService.share.isLoadMore()
            self?.tableView.reloadData()
        }, failure: nil)
    }
    
    func addNewOrder() {
        guard let vc = R.storyboard.main.pAAddNewOrderViewController() else {
            return
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension PAOrdersViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PAOrderService.share.numberOfOrders()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.pAOrderTableViewCell, for: indexPath)! as PAOrderTableViewCell
        guard let order = PAOrderService.share.getOrderAtIndexPath(indexPath: indexPath.row) else {
            return cell
        }
        
        cell.configCellWithData(data: order)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if loadingData && indexPath.row == PAOrderService.share.numberOfOrders() - 1 {
            currentPage+=1
            self.getOrdersAtPage(page: currentPage, reloadData: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = R.storyboard.main.pAOrderDetailViewController() {
            guard let order = PAOrderService.share.getOrderAtIndexPath(indexPath: indexPath.row) else {
                return
            }
            
            vc.orderID = String(order.id)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}
