//
//  PAOrderDetailViewController.swift
//  Parduota
//
//  Created by TungHN on 11/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAOrderDetailViewController: PABaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var ebayIDTitleLabel: UILabel!
    @IBOutlet weak var ebayIDValueLabel: UILabel!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var descriptionValueTextView: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chatTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    
    var orderID: String = TSConstants.EMPTY_STRING
    var order: PAOrderModel = PAOrderModel()
    var messages: Array = [PAMessageModel]()
    var overlayView = UIView()
    var isDisapear = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        overlayView.isHidden = false
        PAOrderService.share.getOrderDetail(orderID: orderID, success: { [weak self] in
            self?.order = PAOrderService.share.order!
            self?.messages = PAOrderService.share.messages
            self?.updateUI(order: (self?.order)!)
            self?.tableView.reloadData()
            self?.overlayView.isHidden = true
            }, failure: nil)
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        isDisapear = true
    }
    
    override func configView() {
        _ = tableView.then {
            $0.dataSource = self
            $0.separatorStyle = .none
            $0.rowHeight = UITableViewAutomaticDimension
            $0.estimatedRowHeight = 44
            $0.layer.cornerRadius = 8
            $0.layer.borderWidth = 0.5
        }
        
        _ = descriptionValueTextView.then {
            $0.layer.cornerRadius = 8
            $0.layer.borderWidth = 0.5
        }
        
        _ = chatTextView.then {
            $0.layer.cornerRadius = 8
            $0.layer.borderWidth = 0.5
        }
        
        _ = statusLabel.then {
            $0.layer.cornerRadius = $0.frame.size.height / 2
            $0.layer.borderWidth = 0.5
            $0.layer.backgroundColor = UIColor.red.cgColor
        }
        
        _ = sendButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.sendMessageAction(message: (self?.chatTextView.text)!)
                })
        }
        
        overlayView.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.size.height)
        overlayView.backgroundColor = .white
        self.view.addSubview(overlayView)
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadConversation), name: NSNotification.Name("order"), object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUI(order: PAOrderModel) {
        valueLabel.text = order.title
        statusLabel.text = order.status
        ebayIDValueLabel.text = order.ebayId
        descriptionValueTextView.text = order.notice
    }
    
    func reloadConversation() {
        PAOrderService.share.pingmessage(success: { [weak self] in
            self?.messages = PAOrderService.share.messages
            self?.tableView.reloadData()
            let lastCellIndexPath = IndexPath(row: (self?.messages.count)! - 1, section: 0)
            self?.tableView.scrollToRow(at: lastCellIndexPath, at: .bottom, animated: true)
        }, failure: nil)
    }
}

// MARK: - Custom method
extension PAOrderDetailViewController {
    
    func sendMessageAction(message: String) {
        if message.trim() == TSConstants.EMPTY_STRING { return }
        PAOrderService.share.addMessage(message: message.trim(), success: { [weak self] in
            self?.messages = PAOrderService.share.messages
            self?.tableView.reloadData()
            let lastCellIndexPath = IndexPath(row: (self?.messages.count)! - 1, section: 0)
            self?.tableView.scrollToRow(at: lastCellIndexPath, at: .bottom, animated: true)
            self?.chatTextView.text = TSConstants.EMPTY_STRING
        }, failure: nil)
    }
    
}

// MARK: - UITableViewDataSource
extension PAOrderDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        if message.type == "admin" {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.pAAdminCell)! as PAAdminCell
            cell.valueLabel.text = message.messages
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.pAUserCell)! as PAUserCell
            cell.valueLabel.text = message.messages
            
            return cell
        }
    }
    
}
