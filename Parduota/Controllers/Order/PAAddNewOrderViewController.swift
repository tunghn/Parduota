//
//  PAAddNewOrderViewController.swift
//  Parduota
//
//  Created by TungHN on 11/30/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAAddNewOrderViewController: PABaseViewController {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var ebayIDLabel: UILabel!
    @IBOutlet weak var ebayIDTextField: UITextField!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    fileprivate var newOrder = PAOrderModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func configView() {
        _ = titleLabel.then {
            $0.text = R.string.localizable.title()
        }
        
        _ = titleTextField.then {
            $0.placeholder = R.string.localizable.title()
            $0.text = newOrder.title
        }
        
        _ = ebayIDLabel.then {
            $0.text = R.string.localizable.title_ebay_id()
        }
        
        _ = ebayIDTextField.then {
            $0.placeholder = R.string.localizable.title_ebay_id()
            $0.text = newOrder.ebayId
        }
        
        _ = descriptionLabel.then {
            $0.text = R.string.localizable.title_description()
        }
        
        _ = descriptionTextView.then {
            $0.delegate = self
            $0.layer.borderColor = UIColor.init(white: 0.5, alpha: 0.5).cgColor
            $0.layer.borderWidth = 0.5
            $0.text = newOrder.notice
        }
        
        _ = saveButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.saveOrder()
                })
        }
    }

}

// MARK: - Action
extension PAAddNewOrderViewController {
    func saveOrder() {
        guard let title = titleTextField.text, title != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input title!").subscribe()
            
            return
        }
        
        guard let ebayID = ebayIDTextField.text, ebayID != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input ebay ID!").subscribe()
            
            return
        }
        
        guard let description = descriptionTextView.text, description != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input description!").subscribe()
            
            return
        }
        
        PAOrderService.share.addNewOrder(title: title, ebayID: ebayID, notice: description,
                                         success: { [weak self] in
                                            self?.navigationController?.popViewController(animated: true)
        }, failure: nil)
    }
}

// MARK: - UITextViewDelegate
extension PAAddNewOrderViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        let newPosition = textView.endOfDocument
        textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
    }
}
