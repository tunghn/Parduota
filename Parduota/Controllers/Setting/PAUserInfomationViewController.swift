//
//  PAUserInfomationViewController.swift
//  Parduota
//
//  Created by TungHN on 12/1/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import DropDown

class PAUserInfomationViewController: PABaseViewController {
    @IBOutlet weak var emailTitleLabel: UILabel!
    @IBOutlet weak var emailValueTextField: UITextField!
    @IBOutlet weak var nicknameTitleLabel: UILabel!
    @IBOutlet weak var nicknameValueTextField: UITextField!
    @IBOutlet weak var genderTitleLabel: UILabel!
    @IBOutlet weak var genderValueTextField: UITextField!
    @IBOutlet weak var genderButton: UIButton!
    @IBOutlet weak var bankAccountTitleLabel: UILabel!
    @IBOutlet weak var bankAccountValueTextField: UITextField!
    @IBOutlet weak var fullNameTitleLabel: UILabel!
    @IBOutlet weak var fullNameValueTextField: UITextField!
    @IBOutlet weak var companyTitleLabel: UILabel!
    @IBOutlet weak var companyValueTextField: UITextField!
    @IBOutlet weak var addressTitleLabel: UILabel!
    @IBOutlet weak var addressValueTextField: UITextField!
    @IBOutlet weak var phoneTitleLabel: UILabel!
    @IBOutlet weak var phoneValueTextField: UITextField!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var descriptionValueTextField: UITextField!
    @IBOutlet weak var otherTitleLabel: UILabel!
    @IBOutlet weak var otherValueTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var user = PAUserModel()
    var listGender = PAAppConstants.GENDER.values.sorted()
    let chooseGender = DropDown()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func configView() {

        let prefs = GlobalContext.get(key: GlobalContext.PREFS)
        user = (prefs?.getUser())!
        
        emailTitleLabel.text = R.string.localizable.title_email()
        nicknameTitleLabel.text = R.string.localizable.title_nickname()
        genderTitleLabel.text = R.string.localizable.title_gender()
        bankAccountTitleLabel.text = R.string.localizable.title_bank_account()
        fullNameTitleLabel.text = R.string.localizable.title_fullname()
        companyTitleLabel.text = R.string.localizable.title_company()
        addressTitleLabel.text = R.string.localizable.title_address()
        phoneTitleLabel.text = R.string.localizable.title_phone()
        descriptionTitleLabel.text = R.string.localizable.title_description()
        otherTitleLabel.text = R.string.localizable.title_other()
        
        _ = emailValueTextField.then {
            $0.placeholder = R.string.localizable.title_email()
            $0.text = user.email
        }
        
        _ = nicknameValueTextField.then {
            $0.placeholder = R.string.localizable.title_nickname()
            $0.text = user.full_name
        }
        
        _ = genderValueTextField.then {
            $0.placeholder = R.string.localizable.title_choose_condition()
            $0.rightViewMode = .always
            let imageView = UIImageView.init(image: R.image.icon_dropdown())
            imageView.frame = CGRect.init(x: 0, y: 0, width: 25, height: 25)
            $0.rightView = imageView
        }
        
        _ = genderButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.view.endEditing(true)
                    self?.actionChooseGender()
                })
        }
        
        _ = bankAccountValueTextField.then {
            $0.delegate = self
            $0.keyboardType = .numberPad
            $0.placeholder = R.string.localizable.title_bank_account()
            $0.text = user.bank_account
        }
        
        _ = fullNameValueTextField.then {
            $0.delegate = self
            $0.placeholder = R.string.localizable.title_fullname()
            $0.text = user.full_name
        }
        
        _ = companyValueTextField.then {
            $0.delegate = self
            $0.placeholder = R.string.localizable.title_company()
            $0.text = user.company
        }
        
        _ = addressValueTextField.then {
            $0.delegate = self
            $0.placeholder = R.string.localizable.title_address()
            $0.text = user.address
        }
        
        _ = phoneValueTextField.then {
            $0.delegate = self
            $0.keyboardType = .numberPad
            $0.placeholder = R.string.localizable.title_phone()
            $0.text = user.phone
        }
        
        _ = descriptionValueTextField.then {
            $0.delegate = self
            $0.placeholder = R.string.localizable.title_description()
            $0.text = user.descriptionField
        }
        
        _ = otherValueTextField.then {
            $0.delegate = self
            $0.placeholder = R.string.localizable.title_other()
            $0.text = user.other
        }
        
        _ = saveButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.view.endEditing(true)
                    self?.saveUserInfo()
                })
        }
        
        setupDropDown()
    }
}

// MARK: - Action
extension PAUserInfomationViewController {
    func setupDropDown() {
        chooseGender.anchorView = genderValueTextField
        chooseGender.direction = .bottom
        chooseGender.dataSource = listGender
        chooseGender.selectionAction = { [unowned self] (index, item) in
            self.genderValueTextField.text = item
            self.user.gender = PAAppConstants.GENDER.allKeysForValue(val: item).first!
        }
    }
    
    func actionChooseGender() {
        chooseGender.show()
    }
    
    func saveUserInfo() {
        guard let bankAccount = bankAccountValueTextField.text?.trim(), bankAccount != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input bank account!").subscribe()
            
            return
        }
        
        user.bank_account = bankAccount
        
        guard let gender = genderValueTextField.text?.trim(), gender != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input gender!").subscribe()
            
            return
        }
        
        user.gender = gender
        
        guard let fullname = fullNameValueTextField.text?.trim() else { return }
        user.full_name = fullname
        
        guard let company = companyValueTextField.text?.trim() else { return }
        user.company = company
        
        guard let address = addressValueTextField.text?.trim() else { return }
        user.address = address
        
        guard let phone = phoneValueTextField.text?.trim() else { return }
        user.phone = phone
        
        guard let description = descriptionValueTextField.text?.trim() else { return }
        user.descriptionField = description
        
        guard let other = otherValueTextField.text?.trim() else { return }
        user.other = other
        
        PAAccountService.share.updateUserInfo(data: user.convertToDictionary(), success: {
            SVProgressHUD.showSuccess(withStatus: "Update infomation success!")
        }, failure: nil)
    }
}

extension PAUserInfomationViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToBeginEditting(view: textField.superview)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToEndEditting(view: textField.superview)
    }
}
