//
//  PASettingViewController.swift
//  Parduota
//
//  Created by TungHN on 11/30/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PASettingViewController: PABaseViewController {
    @IBOutlet weak var updateInfoButton: UIButton!
    @IBOutlet weak var changePasswordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNavigationBarItem()
    }
    
    override func configView() {
        _ = updateInfoButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.gotoUpdateInfo()
                })
        }
        
        _ = changePasswordButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.gotoChangePassWord()
                })
        }
    }
}

// MARK: - Action
extension PASettingViewController {
    func gotoUpdateInfo() {
        guard let vc = R.storyboard.main.pAUserInfomationViewController() else {
            return
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoChangePassWord() {
        guard let vc = R.storyboard.main.pAChangeUserPasswordViewController() else {
            return
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
