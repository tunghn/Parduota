//
//  PAChangeUserPasswordViewController.swift
//  Parduota
//
//  Created by TungHN on 12/1/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAChangeUserPasswordViewController: PABaseViewController {

    @IBOutlet weak var oldPasswordTitleLabel: UILabel!
    @IBOutlet weak var oldPasswordValueTextField: UITextField!
    @IBOutlet weak var passwordTitleLabel: UILabel!
    @IBOutlet weak var passwordValueTextField: UITextField!
    @IBOutlet weak var rePasswordTitleLabel: UILabel!
    @IBOutlet weak var rePasswordValueTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func configView() {
        oldPasswordTitleLabel.text = R.string.localizable.title_old_password()
        passwordTitleLabel.text = R.string.localizable.title_password()
        rePasswordTitleLabel.text = R.string.localizable.title_password_confirmation()
        
        _ = oldPasswordValueTextField.then {
            $0.delegate = self
            $0.placeholder = R.string.localizable.title_old_password()
        }
        
        _ = passwordValueTextField.then {
            $0.delegate = self
            $0.placeholder = R.string.localizable.title_password()
        }
        
        _ = rePasswordValueTextField.then {
            $0.delegate = self
            $0.placeholder = R.string.localizable.title_password_confirmation()
        }
        
        _ = saveButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.view.endEditing(true)
                    self?.saveAction()
                })
        }
    }
}

// MARK: - Action
extension PAChangeUserPasswordViewController {
    func saveAction() {
        guard let oldPassword = oldPasswordValueTextField.text?.trim(), oldPassword != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input old password!").subscribe()
            
            return
        }
        
        guard let password = passwordValueTextField.text?.trim(), password != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input password!").subscribe()
            
            return
        }
        
        guard let rePassword = rePasswordValueTextField.text?.trim(), rePassword != TSConstants.EMPTY_STRING else {
            _ = rx_alertWithTitle(title: "Warning", message: "Please input password confirmation!").subscribe()
            
            return
        }
        
        guard password == rePassword else {
            _ = rx_alertWithTitle(title: "Warning", message: "Password confirmation is invalid!").subscribe()
            
            return
        }
        
        PAAccountService.share.changePassword(oldPassword: oldPassword, newPassword: password, success: {
            SVProgressHUD.showSuccess(withStatus: "Change password success!")
        }, failure: nil)
    }
}

extension PAChangeUserPasswordViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToBeginEditting(view: textField.superview)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToEndEditting(view: textField.superview)
    }
}

