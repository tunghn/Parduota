//
//  PALoginViewController.swift
//  Parduota
//
//  Created by TungHN on 11/2/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit

class PALoginViewController: PABaseViewController {

    @IBOutlet weak var userEmailTextField: UITextField!    
    @IBOutlet weak var userPasswordTextField: UITextField!
    @IBOutlet weak var termsAndConditionButton: UIButton!
    @IBOutlet weak var termAndConditionLabel: UILabel!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signinWithFBButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    var isSelected = false
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func configView() {
       _ = termsAndConditionButton.then {
        $0.isSelected = false
        $0.setImage(R.image.icon_uncheck(), for: .normal)
        $0.setImage(R.image.icon_check(), for: .selected)
        _ = $0.rx.controlEvent(.touchUpInside)
            .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
            .takeUntil($0.rx.deallocated)
            .subscribe(onNext: { [weak self] in
                if let isSelected = self?.isSelected {
                    self?.isSelected = !isSelected
                    self?.termsAndConditionButton.isSelected = !isSelected
                }
            })
        }
        
        _ = termAndConditionLabel.then {
            $0.text = R.string.localizable.term_and_condition_login()
            let gesture = UITapGestureRecognizer.init(target: self, action: #selector(termAndConditionAction))
            $0.addGestureRecognizer(gesture)
        }
        
        _ = signInButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.signinAction()
                })
        }
        
        _ = signUpButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.signupAction()
                })
        }
        
        _ = signinWithFBButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.signinFacebook()
                })
        }
    }
}

// MARK: Action
extension PALoginViewController {
    func signinAction() {
        self.view.endEditing(true)
        guard let userEmail = userEmailTextField.text?.trim(), let userPassword = userPasswordTextField.text?.trim(), !(userEmail.isEmpty || userPassword.isEmpty) else {
            _ = rx_alertWithMessage(message: "Please fill out the form!").subscribe()
            return
        }
        
        guard isSelected else {
            _ = rx_alertWithMessage(message: "You need to agree with our Terms and Condition!").subscribe()
            
            return
        }
        
        PALoginService.share.signin(withUserEmail: userEmail, userPassword: userPassword, success: {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.homeVC()
        }, failure: nil)
    }
    
    func signupAction() {
        guard let vc = R.storyboard.login.signupScreen() else {
            return
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func signinFacebook() {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logOut()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                // if user cancel the login
                if (result?.isCancelled)! {
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData()
                }
            }
        }
    }
    
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    //everything works print the user data
                    let info = result as! [String: AnyObject]
                    var name = info["name"] as? String
                    var email = info["email"] as? String
                    var id = info["id"] as? String
                    
                    if name == nil {
                        name = TSConstants.EMPTY_STRING
                    }
                    
                    if email == nil {
                        email = TSConstants.EMPTY_STRING
                    }
                    
                    if id == nil {
                        id = TSConstants.EMPTY_STRING
                    }
                    
                    PALoginService.share.signinWithSocial(withID: id!, name: name!, email: email!, success: { [weak self] in
                        guard let verified = PALoginService.share.userViewModel?.user?.verified, verified == 1 else {
                            self?.rx_alertWithTitle(title: nil, message: R.string.localizable.message_verify_email()).subscribe().addDisposableTo((self?.disposeBag)!)
                            
                            return
                        }
                        
                        
                    }, failure: nil)
                }
            })
        }
    }
    
    func termAndConditionAction() {
        if let vc = R.storyboard.login.pATermAndConditionViewController() {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension PALoginViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToBeginEditting(view: textField.superview)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToEndEditting(view: textField.superview)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.signinAction()
        
        return true
    }
}
