//
//  PASignUpViewController.swift
//  Parduota
//
//  Created by TungHN on 11/2/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PASignUpViewController: PABaseViewController {
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    @IBOutlet weak var userRePasswordTextField: UITextField!
    @IBOutlet weak var termsAndConditionButton: UIButton!
    @IBOutlet weak var termAndConditionLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    
    var isSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func configView() {
        _ = termsAndConditionButton.then {
            $0.isSelected = false
            $0.setImage(R.image.icon_uncheck(), for: .normal)
            $0.setImage(R.image.icon_check(), for: .selected)
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    if let isSelected = self?.isSelected {
                        self?.isSelected = !isSelected
                        self?.termsAndConditionButton.isSelected = !isSelected
                    }
                })
        }
        
        _ = termAndConditionLabel.then {
            $0.text = R.string.localizable.term_and_condition_login()
            let gesture = UITapGestureRecognizer.init(target: self, action: #selector(termAndConditionAction))
            $0.addGestureRecognizer(gesture)
        }
        
        _ = signUpButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    self?.signUpAction()
                })
        }
        
    }
}

// MARK: - Action
extension PASignUpViewController {
    func signUpAction() {
        self.view.endEditing(true)
        guard let email = userEmailTextField.text?.trim(), PAShareData.shareInstance.isValidEmail(email: email) else {
            _ = rx_alertWithMessage(message: "Email is not correct!", cancelTitle: "OK").subscribe()
            
            return
        }
        
        guard let name = userNameTextField.text?.trim(),
            let password = userPasswordTextField.text?.trim(),
            let rePassword = userRePasswordTextField.text?.trim(),
            !(name == TSConstants.EMPTY_STRING ||
                email == TSConstants.EMPTY_STRING ||
                password == TSConstants.EMPTY_STRING ||
                rePassword == TSConstants.EMPTY_STRING) else {
                    _ = rx_alertWithMessage(message: "Please fill out the form!", cancelTitle: "OK").subscribe()
                    
            return
        }
        
        guard isSelected else {
            _ = rx_alertWithMessage(message: "You need to agree with our Terms and Condition!").subscribe()
            
            return
        }
        
        guard password == rePassword else {
            _ = rx_alertWithMessage(message: "Confirm password is not correct!", cancelTitle: "OK").subscribe()
            
            return
        }
        
        PALoginService.share.signUp(withName: name, email: email, password: password, success: { [weak self] in
            self?.gotoLogin()
        }, failure: nil)
    }
    
    func gotoLogin() {
        _ = rx_alertWithMessage(message: "Sign up success, please login!", cancelTitle: "OK").subscribe(onNext: { (_) in
            R.storyboard.login.loginScreen()?.setAsRootVCAnimated()
        })
    }
    
    func termAndConditionAction() {
        if let vc = R.storyboard.login.pATermAndConditionViewController() {
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

// MARK: - UITextFieldDelegate
extension PASignUpViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToBeginEditting(view: textField.superview)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        PAShareData.shareInstance.changeStateToEndEditting(view: textField.superview)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.signUpAction()
        
        return true
    }
}
