//
//  PATermAndConditionViewController.swift
//  Parduota
//
//  Created by TungHN on 1/2/18.
//  Copyright © 2018 None. All rights reserved.
//

import UIKit

class PATermAndConditionViewController: PABaseViewController {
    
    @IBOutlet weak var ternAndConditionWebView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        PALoginService.share.getTermAndCondition(success: { [weak self] in
            self?.ternAndConditionWebView.loadHTMLString(PALoginService.share.termAndCondition, baseURL: nil)
        }, failure: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
