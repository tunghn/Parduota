//
//  PAMenuViewController.swift
//  Parduota
//
//  Created by TungHN on 11/5/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

enum OrderState: Int {
    case order = 0
    case setting
}

public extension Notification {
    public class PAMenuViewController {
        public static let myNotification = Notification.Name("IndexMenu")
    }
}

class PAMenuViewController: PABaseViewController {

    @IBOutlet weak var tableView: UITableView!
    let notificationName = NSNotification.Name("IndexMenu")
 
    var menuItem = ["All", "Active", "Sold", "Pending", "Draft", "Reject"]
    var menuOrder = ["Order", "Setting"]
    var itemsViewController: UIViewController!
    var orderViewController: UIViewController!
    var settingViewController: UIViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        orderViewController = UINavigationController(rootViewController: R.storyboard.main.pAOrdersViewController()!)
        settingViewController = UINavigationController(rootViewController: R.storyboard.main.pASettingViewController()!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func configView() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func changeViewController(_ menu: Int, _ section: Int) {
        NotificationCenter.default.post(name: notificationName, object: self, userInfo: ["index" : menu])
        if section == 0 {
          self.slideMenuController()?.changeMainViewController(self.itemsViewController, close: true)
        } else if section == 1 {
            switch menu {
            case 0:
                self.slideMenuController()?.changeMainViewController(orderViewController, close: true)
                break
            case 1:
                self.slideMenuController()?.changeMainViewController(settingViewController, close: true)
                break
            default:
                break
            }
        }
    }
}

extension PAMenuViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return menuItem.count
        default:
            return menuOrder.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        switch indexPath.section {
        case 0:
            cell.textLabel?.text = menuItem[indexPath.row]
        default:
            cell.textLabel?.text = menuOrder[indexPath.row]
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.changeViewController(indexPath.row, indexPath.section)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Item"
        default:
            return "Order"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

