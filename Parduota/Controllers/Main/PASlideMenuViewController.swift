//
//  PASlideMenuViewController.swift
//  Parduota
//
//  Created by TungHN on 11/6/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PASlideMenuViewController: SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.changeLeftViewWidth(UIScreen.main.bounds.size.width * 0.6)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
