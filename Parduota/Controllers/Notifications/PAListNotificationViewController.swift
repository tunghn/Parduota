//
//  PAListNotificationViewController.swift
//  Parduota
//
//  Created by TungHN on 12/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import FirebaseMessaging

class PAListNotificationViewController: PABaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var currentPage = 1
    var loadingData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Config FCM
        Messaging.messaging().delegate = self
        let token = Messaging.messaging().fcmToken
        let prefs = GlobalContext.get(key: GlobalContext.PREFS)
        if token != prefs?.getFCMToken() {
            prefs?.updateFCMToken(token: token)
            
            PAAccountService.share.postFCMToken(token_fcm: token!, success: {
                
            }, failure: nil)
        }
        
        // Get notifications
        self.getNotificationsAtPage(page: currentPage, reloadData: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func configView() {
        _ = tableView.then {
            $0.dataSource = self
            $0.delegate = self
            $0.separatorStyle = .none
        }
    }

}

// MARK: - MessagingDelegate
extension PAListNotificationViewController: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        
        
    }
}

// MARK: - Action
extension PAListNotificationViewController {
    func getNotificationsAtPage(page: Int, reloadData: Bool) {
        tableView.isHidden = true
        PANotificationService.share.getNotificationsWithPage(page: page, isReloadData: reloadData, success: { [weak self] in
            self?.loadingData = PANotificationService.share.isLoadMore()
            self?.tableView.isHidden = false
            self?.tableView.reloadData()
            
        }, failure: nil)
    }
}

// MARK: UITableViewDelegate, UITableViewDataSource
extension PAListNotificationViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PANotificationService.share.numberOfNotifications()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.pANotificationCell, for: indexPath)! as PANotificationCell
        guard let notification = PANotificationService.share.getNotificationAtIndexPath(indexPath: indexPath.row) else {
            return cell
        }
        
        cell.configCellWithData(data: notification)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if loadingData && indexPath.row == PANotificationService.share.numberOfNotifications() - 1 {
            currentPage+=1
                self.getNotificationsAtPage(page: currentPage, reloadData: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let notification = PANotificationService.share.getNotificationAtIndexPath(indexPath: indexPath.row) else {
            return
        }
        
        notification.readed = 1
        switch notification.type {
        case 0, 1, 2, 6, 7, 11, 12, 13: do {
            if let vc = R.storyboard.main.pADetailItemViewController() {
                vc.item = notification.item
                navigationController?.pushViewController(vc, animated: true)
            }
        }
            break
        case 18, 19, 20: do {
            if let vc = R.storyboard.main.pAOrderDetailViewController() {
                vc.orderID = PANotificationService.share.getOrderID(notification: notification)
                navigationController?.pushViewController(vc, animated: true)
            }
        }
            break
        default:
            break
        }
        
    }
}
