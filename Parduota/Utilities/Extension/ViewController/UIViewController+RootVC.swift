//
//  UIViewController+RootVC.swift
//  Club
//
//  Created by yersini on 4/24/17.
//  Copyright © 2017 Ace Leisure Pte Ltd. All rights reserved.
//

import Foundation

extension UIViewController {
    // MARK: - Transit Root View Controller
    /**
     Change application's rootview controller with current viewcontroller with CrossDissolve transition
     */
    func setAsRootVCAnimated() {
        let originalVC = UIApplication.shared.keyWindow?.rootViewController
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        UIView.transition(with: window, duration: 0.5, options: .transitionCrossDissolve, animations: { () -> Void in
            let oldState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            window.rootViewController = self
            UIView.setAnimationsEnabled(oldState)
        }){ _ in
            originalVC?.dismiss(animated: false, completion: nil)
        }
    }
    
    /**
     Set as root view controller
     */
    func setAsRoot() {
        //Remember original root vc first.
        let originalVC = UIApplication.shared.keyWindow?.rootViewController
        UIApplication.shared.keyWindow?.rootViewController = self
        
        // After this, dismiss all original VC's stack
        originalVC?.dismiss(animated: false, completion: nil)
    }
}
