//
//  UIviewController+Utility.swift
//  YerStuVC
//
//  Created by yersini on 10/18/16.
//  Copyright © 2016 Yersini. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import UIKit

// MARK: - Alert Style
extension UIViewController {
    // MARK: - Show alert controller: simply alert controller
    /**
    Display Alert Controller to user (simlpy use presentViewController for now), alertController should be configured with callbacks first.
    - Parameter alert: UIAlertController to be displayed on top of this viewcontroller instance.
    */
    func showAlertController(alert:UIAlertController){
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Rx Extension for showing alert view controllers.
    /**
    Rx convenient function for display alert, .Next, .Completed delivered.
    - Returns: Observable\<AlertControllerResult\>
    */
    func rx_alertWithTitle(title:String?, message:String?, cancelTitle ct:String?, destructiveTitle dt:String?, otherTitles ot:[String]) -> Observable<AlertControllerResult>{
        let ob:Observable<AlertControllerResult> = Observable.create{[weak self] observer -> Disposable in
            guard let s = self else {
                observer.on(.completed)
                return Disposables.create()
            }
            let alert = UIAlertController.alertWithTitle(title: title, message: message, cancelTitle: ct, destructiveTitle:dt, otherTitles: ot, callback: { (result) -> Void in
                observer.on(.next(result))
                observer.on(.completed)
            })
            s.showAlertController(alert: alert)
            return Disposables.create {
                alert.dismiss(animated: true, completion: nil)
            }
        }
        return ob.observeOn(MainScheduler.instance)
    }
    
    /**
     Rx convenient function for display alert, .Next, .Completed delivered.
     - Returns: Observable\<AlertControllerResult\>
     */
    func rx_alertWithTitle(title:String?, message:String?, cancelTitle ct:String?, otherTitles:[String]) -> Observable<AlertControllerResult>{
        return rx_alertWithTitle(title: title, message: message, cancelTitle: ct, destructiveTitle: nil, otherTitles: otherTitles)
    }
    
    /**
     Rx convenient function for display alert, .Next, .Completed delivered.
     - Returns: Observable\<AlertControllerResult\>
     */
    func rx_alertWithMessage(message:String?, cancelTitle ct:String?, otherTitles:[String]) -> Observable<AlertControllerResult>{
        return rx_alertWithTitle(title: nil, message: message, cancelTitle: ct, destructiveTitle: nil, otherTitles: otherTitles)
    }
    
    /**
     Rx convenient function for display alert, .Next, .Completed delivered.
     - Returns: Observable\<AlertControllerResult\>
     */
    func rx_alertWithMessage(message:String?, cancelTitle ct:String) -> Observable<()>{
        return rx_alertWithTitle(title: nil, message: message, cancelTitle: ct, destructiveTitle: nil, otherTitles: []).map{ _, _ in
            return ()
        }
    }
    
    /**
     Rx convenient function for display alert, .Next, .Completed delivered.
     - Returns: Observable\<()\>
     */
    func rx_alertWithMessage(message:String?) -> Observable<()>{
        return rx_alertWithTitle(title: nil, message: message, cancelTitle: "OK", destructiveTitle: nil, otherTitles: []).map{ _, _  in
            return ()
        }
    }
    
    /**
     Rx convenient function for display alert, .Next, .Completed delivered.
     - Returns: Observable\<()\>
    */
    func rx_alertWithTitle(title:String?, message:String?) -> Observable<()>{
        return rx_alertWithTitle(title: title, message: message, cancelTitle: "OK", otherTitles: []).map{_, _ -> () in
            return ()
        }
    }
}

// MARK: - ActionSheetStyle
extension UIViewController{
    /**
     Rx convenient function for displaying action sheet from source view.
    */
    func rx_actionSheetFromView(sourceView:UIView, title:String? = nil, cancelTitle ct:String? = nil, destructiveTitle dt:String? = nil, otherTitles:[String] = []) -> Observable<AlertControllerResult>{
        let ob:Observable<AlertControllerResult> = Observable.create{[weak self] observer -> Disposable in
            guard let s = self else {
                observer.on(.completed)
                return Disposables.create()
            }
            let alert = UIAlertController.actionSheetWithTitle(title: title, cancelTitle: ct, destructiveTitle:dt, otherTitles: otherTitles, callback: { (result) -> Void in
                observer.on(.next(result))
                observer.on(.completed)
            })
            alert.popoverPresentationController?.sourceView = sourceView
            s.showAlertController(alert: alert)
            return Disposables.create {
                alert.dismiss(animated: true, completion: nil)
            }
        }
        return ob.observeOn(MainScheduler.instance)
    }
    
    /**
     Rx convenient function for displaying action sheet from bar button item
     */
    func rx_actionSheetFromBarButtonItem(barButtonItem:UIBarButtonItem, title:String? = nil, cancelTitle ct:String? = nil, destructiveTitle dt:String? = nil, otherTitles:[String] = []) -> Observable<AlertControllerResult>{
        let ob:Observable<AlertControllerResult> = Observable.create{[weak self] observer -> Disposable in
            guard let s = self else {
                observer.on(.completed)
                return Disposables.create()
            }
            let alert = UIAlertController.actionSheetWithTitle(title: title, cancelTitle: ct, destructiveTitle:dt, otherTitles: otherTitles, callback: { (result) -> Void in
                observer.on(.next(result))
                observer.on(.completed)
            })
            alert.popoverPresentationController?.barButtonItem = barButtonItem
            s.showAlertController(alert: alert)
            return Disposables.create {
                alert.dismiss(animated: true, completion: nil)
            }
        }
        return ob.observeOn(MainScheduler.instance)
    }
}

// MARK: - ElsaAlertExtension
enum ElsaAlertVCResult{
    case Clicked(Int) // When click any button
    case Cancelled    // When tap close button
}
