//
//  Dictionary+Extention.swift
//  Parduota
//
//  Created by TungHN on 11/22/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation

extension Dictionary where Value : Equatable {
    func allKeysForValue(val : Value) -> [Key] {
        return self.filter { $1 == val }.map { $0.0 }
    }
}
