//
//  PATargetType.swift
//  Parduota
//
//  Created by TungHN on 11/3/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Moya
import MoyaSugar

public protocol PATargetType: SugarTargetType {
    
}

extension PATargetType {
    
    public var baseURL: URL {
        return URL(string: PAAppConstants.API_HOST)!
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        return .request
    }
}

