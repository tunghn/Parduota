//
//  PALoginService.swift
//  Parduota
//
//  Created by TungHN on 11/3/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class PALoginService {
    static let share = PALoginService()
    private init(){}
    open var userViewModel: PAUserViewModel? = PAUserViewModel()
    open var termAndCondition: String = TSConstants.EMPTY_STRING
    fileprivate let provider = TSApiManager.shared.createProviderFor(PALoginAPI.self)
}

extension PALoginService {
    open func signin(withUserEmail userEmail: String, userPassword: String, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        TSApiManager.shared.request(api: PALoginAPI.signin(userEmail: userEmail, userPassword: userPassword),
                                    provider: provider,
                                    nextHandler: ({ [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        
                                        // NSUserDataDefault
                                        let prefs = GlobalContext.get(key: GlobalContext.PREFS)
                                        
                                        // Save token
                                        self.userViewModel?.token = json[PAUserViewModel.PropertiesKey.token.rawValue].string
                                        prefs?.updateUserAccessToken(token: self.userViewModel?.token)
                                        
                                        if let user = json[PAUserViewModel.PropertiesKey.user.rawValue].dictionaryObject {
                                            // Save Club Owner data
                                            self.userViewModel?.user = Mapper<PAUserModel>().map(JSONObject: user)
                                            prefs?.updateUserDataJSON(user: self.userViewModel?.user)
                                        }
                                        
                                        success?()
        }),
                                    errorHandle: { (respone) in
                                        DefaultRequestFailureHandler(respone)
        })
    }
    
    open func signUp(withName name: String, email: String, password: String, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        TSApiManager.shared.request(api: PALoginAPI.signup(email: email, name: name, password: password),
                                    provider: provider,
                                    nextHandler: ({ (json) in
                                        SVProgressHUD.dismiss()
        }),
                                    errorHandle: { (response) in
                                        DefaultRequestFailureHandler(response)
        })
    }
    
    open func signinWithSocial(withID id: String, name: String, email: String, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        TSApiManager.shared.request(api: PALoginAPI.signinWithSocial(name: name, email: email, providerID: id, provider: PAAppConstants.FACEBOOK, password: TSConstants.EMPTY_STRING),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        // NSUserDataDefault
                                        let prefs = GlobalContext.get(key: GlobalContext.PREFS)
                                        
                                        if let user = json[PAUserViewModel.PropertiesKey.user.rawValue].dictionaryObject {
                                            // Save Club Owner data
                                            self.userViewModel?.user = Mapper<PAUserModel>().map(JSONObject: user)
                                            prefs?.updateUserDataJSON(user: self.userViewModel?.user)
                                        }
                                        
                                        // Save token
                                        if let verified = PALoginService.share.userViewModel?.user?.verified, verified == 1 {
                                            self.userViewModel?.token = json[PAUserViewModel.PropertiesKey.token.rawValue].string
                                            prefs?.updateUserAccessToken(token: self.userViewModel?.token)
                                        }
                                        
                                        success?()
                                        
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func getTermAndCondition(success: PARequestSuccessClosure? = nil, failure: PARequestSuccessWithDataClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PALoginAPI.getTermAndCondition(authorization: token, country: "term_en"),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        let data = json[PAAppConstants.DATA]
                                        self.termAndCondition = String(describing: data)
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
}
