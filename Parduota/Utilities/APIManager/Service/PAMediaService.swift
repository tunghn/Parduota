//
//  PAMediaService.swift
//  Parduota
//
//  Created by TungHN on 11/28/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
import Alamofire

class PAMediaService {
    static let share = PAMediaService()
    private init(){}
    fileprivate let provider = TSApiManager.shared.createProviderFor(PAMediaAPI.self)
    open var image: PAMediaModel? = PAMediaModel()
}

extension PAMediaService {
    open func uploadImage(image: Data, success: PARequestSuccessWithDataClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAMediaAPI.uploadImage(authorization: token, image: image),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        let dataMedia = json["medium"]
                                        if let image = dataMedia.dictionaryObject {
                                            self.image = Mapper<PAMediaModel>().map(JSONObject: image)
                                        }
                                        
                                        success!(self.image!)
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func deleteImage(imageID: Int, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAMediaAPI.deleteImage(authorization: token, imageId: imageID),
                                    provider: provider,
                                    nextHandler: { (json) in
                                        SVProgressHUD.dismiss()
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
}
