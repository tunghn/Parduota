//
//  PAItemService.swift
//  Parduota
//
//  Created by TungHN on 11/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class PAItemService {
    static let share = PAItemService()
    private init(){}
    open var itemViewModel: PAItemViewModel? = PAItemViewModel()
    open var itemsList: Array = [PAItemModel]()
    fileprivate let provider = TSApiManager.shared.createProviderFor(PAItemAPI.self)
    open var newItem: PAItemModel? = PAItemModel()
}

extension PAItemService {
    open func getItemsListWithPage(page: Int, isReloadData: Bool, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAItemAPI.getItemsList(authorization: token, page: page),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        self.itemViewModel?.current_page = json[PAItemViewModel.PropertiesKey.current_page.rawValue].int
                                        self.itemViewModel?.last_page = json[PAItemViewModel.PropertiesKey.last_page.rawValue].int
                                        if let itemsList = json[PAItemViewModel.PropertiesKey.data.rawValue].arrayObject {
                                            if let data = Mapper<PAItemModel>().mapArray(JSONObject: itemsList) {
                                                if isReloadData {
                                                    self.itemsList.removeAll()
                                                }
                                                
                                                self.itemsList.append(contentsOf: data)
                                            }
                                        }
                                        
                                        success?()
                                        
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func getItemsList(page: Int, status: Int, isReloadData: Bool, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAItemAPI.getItemByStatus(authorization: token, status: status, page: page),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        self.itemViewModel?.current_page = json[PAItemViewModel.PropertiesKey.current_page.rawValue].int
                                        self.itemViewModel?.last_page = json[PAItemViewModel.PropertiesKey.last_page.rawValue].int
                                        if let itemsList = json[PAItemViewModel.PropertiesKey.data.rawValue].arrayObject {
                                            if let data = Mapper<PAItemModel>().mapArray(JSONObject: itemsList) {
                                                if isReloadData {
                                                    self.itemsList.removeAll()
                                                }
                                                
                                                self.itemsList.append(contentsOf: data)
                                            }
                                        }
                                        
                                        success?()
                                        
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func addNewItem(item: [String: Any], success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAItemAPI.addNewItem(authorization: token, item: item),
                                    provider: provider,
                                    nextHandler: { (json) in
                                        SVProgressHUD.dismiss()
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
}

// MARK: Method
extension PAItemService {
    open func numberOfItems() -> Int{
        return self.itemsList.count
    }
    
    open func getItemAtIndexPath(indexPath: Int) -> PAItemModel? {
        if indexPath < self.itemsList.count {
            return self.itemsList[indexPath]
        }
        
        return nil
    }
    
    open func isLoadMore() -> Bool {
        if self.itemViewModel?.current_page == self.itemViewModel?.last_page {
            return false
        }
        
        return true
    }
    
    open func getNewItem() -> PAItemModel {
        newItem = PAItemModel()
        
        return newItem!
    }
}
