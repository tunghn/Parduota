//
//  PAAccountService.swift
//  Parduota
//
//  Created by TungHN on 12/1/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper
import Alamofire

class PAAccountService {
    static let share = PAAccountService()
    private init(){}
    fileprivate let provider = TSApiManager.shared.createProviderFor(PAAccountAPI.self)
    fileprivate var user: PAUserModel? = PAUserModel()
}

extension PAAccountService {
    open func updateUserInfo(data: [String: Any], success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAAccountAPI.updateUserInfo(authorization: token, data: data),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        if let user = json[PAUserViewModel.PropertiesKey.user.rawValue].dictionaryObject {
                                            let prefs = GlobalContext.get(key: GlobalContext.PREFS)
                                            self.user = Mapper<PAUserModel>().map(JSONObject: user)
                                            prefs?.updateUserDataJSON(user: self.user)
                                        }
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func changePassword(oldPassword: String, newPassword: String, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAAccountAPI.changePassword(authorization: token, oldPassword: oldPassword, newPassword: newPassword, rePassword: newPassword),
                                    provider: provider,
                                    nextHandler: { (json) in
                                        SVProgressHUD.dismiss()
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func postFCMToken(token_fcm: String, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAAccountAPI.postFCMToken(authorization: token, token_fcm: token_fcm, type: "ios"),
                                    provider: provider,
                                    nextHandler: { (json) in
                                        
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
}
