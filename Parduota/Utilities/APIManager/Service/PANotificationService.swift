//
//  PANotificationService.swift
//  Parduota
//
//  Created by TungHN on 12/10/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class PANotificationService {
    static let share = PANotificationService()
    private init(){}
    open var notificationViewModel: PAItemViewModel? = PAItemViewModel()
    open var notificationsList: Array = [PANotificationModel]()
    open var orderID: String = TSConstants.EMPTY_STRING
    fileprivate let provider = TSApiManager.shared.createProviderFor(PANotificationAPI.self)
}

extension PANotificationService {
    open func getNotificationsWithPage(page: Int, isReloadData: Bool, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PANotificationAPI.getNotifications(authorization: token, page: page),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        self.notificationViewModel?.current_page = json[PAItemViewModel.PropertiesKey.current_page.rawValue].int
                                        self.notificationViewModel?.last_page = json[PAItemViewModel.PropertiesKey.last_page.rawValue].int
                                        if let notificationsList = json[PAItemViewModel.PropertiesKey.data.rawValue].arrayObject {
                                            if let data = Mapper<PANotificationModel>().mapArray(JSONObject: notificationsList) {
                                                if isReloadData {
                                                    self.notificationsList.removeAll()
                                                }
                                                
                                                self.notificationsList.append(contentsOf: data)
                                            }
                                        }
                                        self.mappingData()
                                        
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
}

// MARK: - Method

extension PANotificationService {
    
    open func numberOfNotifications() -> Int{
        return self.notificationsList.count
    }
    
    open func getNotificationAtIndexPath(indexPath: Int) -> PANotificationModel? {
        if indexPath < self.notificationsList.count {
            return self.notificationsList[indexPath]
        }
        
        return nil
    }
    open func isLoadMore() -> Bool {
        if self.notificationViewModel?.current_page == self.notificationViewModel?.last_page {
            return false
        }
        
        return true
    }
    
    func mappingData() {
        for notification in self.notificationsList {
            switch notification.type {
            case 0, 1, 2, 6, 7, 11, 12, 13: do {
                if let data = Mapper<PAItemModel>().map(JSONObject: notification.meta_data) {
                    notification.item = data
                }
            }
                break
            case 19: do {
                if let orderID = notification.meta_data["order_id"] {
                    self.orderID = orderID as! String
                }
            }
                break
            case 18, 20: do {
                if let orderID = notification.meta_data[PABaseDataModel.PropertiesKey.id.rawValue] {
                    if let strID = orderID {
                        self.orderID = String(describing: strID)
                    }
                }
            }
                break
            default:
                break
            }
        }
    }
    
    open func getOrderID(notification: PANotificationModel) -> String{
        switch notification.type {
        case 19: do {
            if let orderID = notification.meta_data["order_id"] {
                if let strOrder = orderID {
                    return String(describing: strOrder)
                }
            }
        }
            break
        case 18, 20: do {
            if let orderID = notification.meta_data[PABaseDataModel.PropertiesKey.id.rawValue] {
                if let strID = orderID {
                    return String(describing: strID)
                }
            }
        }
            break
        default:
            break
        }
        
        return TSConstants.EMPTY_STRING
    }
}
