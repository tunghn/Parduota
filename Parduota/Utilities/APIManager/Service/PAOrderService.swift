//
//  PAOrderService.swift
//  Parduota
//
//  Created by TungHN on 11/5/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class PAOrderService {
    static let share = PAOrderService()
    private init(){}
    open var ordersList: PAOrdersListModel? = PAOrdersListModel()
    open var allData: Array = [PAOrderModel]()
    open var order: PAOrderModel? = PAOrderModel()
    open var messages: Array = [PAMessageModel]()
    fileprivate let provider = TSApiManager.shared.createProviderFor(PAOrderAPI.self)
}

// MARK: API
extension PAOrderService {
    open func getOrdersListAtPage(page: Int, isReloadData: Bool, success: PARequestSuccessClosure? = nil, failure: PARequestFailureClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAOrderAPI.getOrdersList(authorization: token, page: page),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        
                                        if let ordersList = Mapper<PAOrdersListModel>().map(JSONObject: json.dictionaryObject) {
                                            self.ordersList = ordersList
                                            if isReloadData {
                                                self.allData.removeAll()
                                            }
                                            
                                            self.allData.append(contentsOf: ordersList.data)
                                        }
                                        
                                        success?()
        }, errorHandle: { (response) in
            DefaultRequestFailureHandler(response)
        })
    }
    
    open func getOrderDetail(orderID: String, success: PARequestSuccessClosure? = nil, failure: PARequestSuccessWithDataClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAOrderAPI.getOrderDetail(authorization: token, orderID: orderID),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        if self.messages.count > 0 {
                                            self.messages.removeAll()
                                        }
                                        
                                        if let order = json[PAAppConstants.ORDER].dictionaryObject {
                                            self.order = Mapper<PAOrderModel>().map(JSONObject: order)
                                        }
                                        
                                        if let messages = json[PAAppConstants.MESSAGES].arrayObject {
                                            if let data = Mapper<PAMessageModel>().mapArray(JSONObject: messages) {
                                                self.messages.append(contentsOf: data)
                                            }
                                        }
                                        
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func addNewOrder(title: String, ebayID: String, notice: String, success: PARequestSuccessClosure? = nil, failure: PARequestSuccessWithDataClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        TSApiManager.shared.request(api: PAOrderAPI.addNewOrder(authorization: token, title: title, ebayID: ebayID, notice: notice),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        if let order = json[PAAppConstants.ORDER].dictionaryObject {
                                            self.order = Mapper<PAOrderModel>().map(JSON: order)
                                        }
                                        
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func addMessage(message: String, success: PARequestSuccessClosure? = nil, failure: PARequestSuccessWithDataClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        guard let orderID = self.order?.id else { return }
        TSApiManager.shared.request(api: PAOrderAPI.addMessage(authorization: token, orderID: String(orderID), message: message),
                                    provider: provider,
                                    nextHandler: { [unowned self] (json) in
                                        SVProgressHUD.dismiss()
                                        if self.messages.count > 0 {
                                            self.messages.removeAll()
                                        }
                                        
                                        if let messages = json[PAAppConstants.MESSAGES].arrayObject {
                                            if let data = Mapper<PAMessageModel>().mapArray(JSONObject: messages) {
                                                self.messages.append(contentsOf: data)
                                            }
                                        }
                                        
                                        success?()
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
    
    open func pingmessage(success: PARequestSuccessClosure? = nil, failure: PARequestSuccessWithDataClosure? = nil) {
        SVProgressHUD.show()
        guard let prefs = GlobalContext.get(key: GlobalContext.PREFS) else { return }
        let token = prefs.getUserAccessToken()
        guard let orderID = self.order?.id else { return }
        TSApiManager.shared.request(api: PAOrderAPI.pingMessage(authorization: token, orderID: String(orderID)),
                                    provider: provider,
                                    nextHandler: { (json) in
                                        SVProgressHUD.dismiss()
                                        if self.messages.count > 0 {
                                            self.messages.removeAll()
                                        }
                                        
                                        if let messages = json.arrayObject {
                                            if let data = Mapper<PAMessageModel>().mapArray(JSONObject: messages) {
                                                self.messages.append(contentsOf: data)
                                            }
                                        }
                                        
                                        success?()
                                        
        }) { (response) in
            DefaultRequestFailureHandler(response)
        }
    }
}

// MARK: Method
extension PAOrderService {
    open func numberOfOrders() -> Int{
        return self.allData.count
    }
    
    open func getOrderAtIndexPath(indexPath: Int) -> PAOrderModel? {
        if indexPath < self.allData.count {
            return self.allData[indexPath]
        }
        
        return nil
    }
    
    open func isLoadMore() -> Bool {
        if self.ordersList?.currentPage == self.ordersList?.lastPage {
            return false
        }
        
        return true
    }
}
