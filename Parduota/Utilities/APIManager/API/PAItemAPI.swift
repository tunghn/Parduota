//
//  PAItemAPI.swift
//  Parduota
//
//  Created by TungHN on 11/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Moya
import MoyaSugar

enum PAItemAPI {
    case getItemsList(authorization: String, page: Int)
    case getItemByStatus(authorization: String, status: Int, page: Int)
    case addNewItem(authorization: String, item: [String: Any])
    case cancelItem(authorization: String, itemId: Int)
}

extension PAItemAPI: PATargetType {
    var route: Route {
        switch self {
        case .getItemsList( _, _):
            return .get("get_items")
        case .getItemByStatus( _, let status, _):
            return .get("get_item_by_status/\(status)")
        case .addNewItem(_ , _):
            return .post("item/add")
        case .cancelItem( _, let itemId):
            return .post("item/cancel/\(itemId)")
        }
    }
    
    var params: Parameters? {
        switch self {
        case .getItemsList(_, let page):
            return URLEncoding.default => [
                "page" : page
            ]
        case .getItemByStatus(_, _, let page):
            return URLEncoding.default => [
                "page" : page
            ]
        case .addNewItem(_, let item):
            return JSONEncoding.default => item
        default:
            return nil
        }
    }
    
    var httpHeaderFields: [String: String]? {
        switch self {
        case .getItemsList(let authorization, _):
            return ["Authorization" : "Bearer \(authorization)"]
        case .getItemByStatus(let authorization, _, _):
            return ["Authorization" : "Bearer \(authorization)"]
        case .addNewItem(let authorization, _):
            return ["Authorization" : "Bearer \(authorization)"]
        case .cancelItem(let authorization, _):
            return ["Authorization" : "Bearer \(authorization)"]
        }
    }
}
