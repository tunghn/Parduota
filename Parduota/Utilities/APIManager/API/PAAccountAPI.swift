//
//  PAAccountAPI.swift
//  Parduota
//
//  Created by TungHN on 12/1/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Moya
import MoyaSugar

enum PAAccountAPI {
    case updateUserInfo(authorization: String, data: [String: Any])
    case changePassword(authorization: String, oldPassword: String, newPassword: String, rePassword: String)
    case postFCMToken(authorization: String, token_fcm: String, type: String)
}

extension PAAccountAPI: PATargetType {
    var route: Route {
        switch self {
        case .updateUserInfo( _, _):
            return .post("update_profile")
        case .changePassword( _, _, _, _):
            return .post("update/password")
        case .postFCMToken(_, _, _):
            return .post("user/fcm")
        }
    }
    
    var params: Parameters? {
        switch self {
        case .updateUserInfo(_, let data):
            return JSONEncoding.default => data
        case .changePassword(_, let oldPassword, let newPassword, let rePassword):
            return JSONEncoding.default => [
                "old_password" : oldPassword,
                "password" : newPassword,
                "password_confirmation" : rePassword
            ]
        case .postFCMToken(_,let token_fcm,let type):
            return JSONEncoding.default => [
                "token_fcm" : token_fcm,
                "type" : type
            ]
        }
    }
    
    var httpHeaderFields: [String: String]? {
        switch self {
        case .updateUserInfo(let authorization, _):
            return ["Authorization" : "Bearer \(authorization)"]
        case .changePassword(let authorization, _, _, _):
            return ["Authorization" : "Bearer \(authorization)"]
        case .postFCMToken(let authorization, _, _):
            return ["Authorization" : "Bearer \(authorization)"]
        }
    }
}
