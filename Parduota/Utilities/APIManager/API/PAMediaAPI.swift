//
//  PAMediaAPI.swift
//  Parduota
//
//  Created by TungHN on 11/28/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Moya
import MoyaSugar

enum PAMediaAPI {
    case uploadImage(authorization: String, image: Data)
    case deleteImage(authorization: String, imageId: Int)
}

extension PAMediaAPI: PATargetType {
    var route: Route {
        switch self {
        case .uploadImage( _, _):
            return .post("media/upload")
        case .deleteImage( _, let imageId):
            return .delete("media/delete/\(imageId)")
        }
    }
    
    var params: Parameters? {
        switch self {
        default:
            return nil
        }
    }
    
    var multipartBody: [MultipartFormData]? {
        switch self {
        case .uploadImage(_, let image):
            return [MultipartFormData(provider: .data(image), name: "media", fileName: "photo.png", mimeType: "image/png")]
        default:
            return nil
        }
    }
    
    var httpHeaderFields: [String: String]? {
        switch self {
        case .uploadImage(let authorization, _):
            return ["Authorization" : "Bearer \(authorization)"]
        case .deleteImage(let authorization, _):
            return ["Authorization" : "Bearer \(authorization)"]
        }
    }
    
    var task: Task {
        switch self {
        case .uploadImage(_, _):
            return .upload(UploadType.multipart(multipartBody!))
        default:
            return .request
        }
    }
}
