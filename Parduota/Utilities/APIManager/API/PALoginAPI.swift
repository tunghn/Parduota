//
//  PALoginAPI.swift
//  Parduota
//
//  Created by TungHN on 11/3/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import Moya
import MoyaSugar

enum PALoginAPI {
    case signin(userEmail: String, userPassword: String)
    case signinWithSocial(name: String, email: String, providerID: String, provider: String, password: String)
    case signup(email: String, name: String, password: String)
    case requestVIP(fullName: String, company: String, address: String, phone: String, viber: String, description: String, other: String)
    case getTermAndCondition(authorization: String, country: String)
}

extension PALoginAPI: PATargetType {
    
    var route: Route {
        switch self {
        case .signin:
            return .post("auth/login")
        case .signinWithSocial:
            return .post("auth/login_social")
        case .signup:
            return .post("auth/signup")
        case .requestVIP:
            return .post("request_vip")
        case .getTermAndCondition:
            return .get("get_setting")
        }
    }
        
    var params: Parameters? {
        switch self {
        case .signin(let userEmail, let userPassword):
            return JSONEncoding.default => [
                "email": userEmail,
                "password": userPassword,
            ]
        case .signinWithSocial(let name, let email, let providerID, let provider, let password):
            return JSONEncoding.default => [
                "name": name,
                "email": email,
                "provider_id": providerID,
                "provider": provider,
                "password": password
            ]
        case .signup(let email, let name, let password):
            return JSONEncoding.default => [
                "name": name,
                "email": email,
                "password": password
            ]
        case .requestVIP(let fullName, let company, let address, let phone, let viber, let description, let other):
            return JSONEncoding.default => [
                "fullname": fullName,
                "company": company,
                "address": address,
                "phone": phone,
                "viber": viber,
                "description": description,
                "other": other
            ]
        case .getTermAndCondition(_, let country):
            return URLEncoding.default => [
                "type": country
            ]
        }
    }
    
    var httpHeaderFields: [String: String]? {
        switch self {
        case .getTermAndCondition(let authorization, _):
            return ["Authorization": "Bearer \(authorization)"]
        default:
            return nil
        }
    }
}
