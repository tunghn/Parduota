//
//  PAOrderAPI.swift
//  Parduota
//
//  Created by TungHN on 11/3/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Moya
import MoyaSugar

enum PAOrderAPI {
    case getOrdersList(authorization: String, page: Int)
    case addNewOrder(authorization: String, title: String, ebayID: String, notice: String)
    case getOrderDetail(authorization: String, orderID: String)
    case addMessage(authorization: String, orderID: String, message: String)
    case pingMessage(authorization: String, orderID: String)
}

extension PAOrderAPI: PATargetType {
    var route: Route {
        switch self {
        case .getOrdersList:
            return .get("get_orders")
        case .addNewOrder:
            return .post("orders/add")
        case .getOrderDetail(_, let orderID):
            return .get("orders/view/\(orderID)")
        case .addMessage(_, let orderID, _):
            return .post("orders/add_messages/\(orderID)")
        case .pingMessage(_, let orderID):
            return .get("orders/ping_messages/\(orderID)")
        }
    }
    
    var params: Parameters? {
        switch self {
        case .getOrdersList(_, let page):
            return URLEncoding.default => [
                "page" : page
            ]
        case .addNewOrder(_, let title, let ebayID, let notice):
            return JSONEncoding.default => [
                "title": title,
                "ebay_id": ebayID,
                "notice": notice
            ]
        case .addMessage(_, _, let message):
            return JSONEncoding.default => [
                "message": message
            ]
        default:
            return nil
        }
    }
    
    var httpHeaderFields: [String: String]? {
        switch self {
        case .addNewOrder(let authorization, _, _, _):
            return ["Authorization": "Bearer \(authorization)"]
        case .getOrdersList(let authorization, _):
            return ["Authorization": "Bearer \(authorization)"]
        case .getOrderDetail(let authorization, _):
            return ["Authorization": "Bearer \(authorization)"]
        case .addMessage(let authorization, _, _):
            return ["Authorization": "Bearer \(authorization)"]
        case .pingMessage(let authorization, _):
            return ["Authorization": "Bearer \(authorization)"]
        }
    }
}
