//
//  PANotificationAPI.swift
//  Parduota
//
//  Created by TungHN on 12/10/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import Moya
import MoyaSugar

enum PANotificationAPI {
    case getNotifications(authorization: String, page: Int)
    case updateNotificationReaded(authorization: String, id: Int)
}

extension PANotificationAPI: PATargetType {
    var route: Route {
        switch self {
        case .getNotifications(_, _):
            return .get("get_notification")
        case .updateNotificationReaded(_, let id):
            return .post("readed_notification/\(id)")
        }
    }
    
    var params: Parameters? {
        switch self {
        case .getNotifications(_, let page):
            return URLEncoding.default => [
                "page" : page
            ]
        default:
            return nil
        }
    }
    
    var httpHeaderFields: [String: String]? {
        switch self {
        case .getNotifications(let authorization, _):
            return ["Authorization" : "Bearer \(authorization)"]
        case .updateNotificationReaded(let authorization, _):
            return ["Authorization" : "Bearer \(authorization)"]
        }
    }
}
