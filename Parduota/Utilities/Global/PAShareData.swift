//
//  PAShareData.swift
//  Parduota
//
//  Created by TungHN on 11/2/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAShareData: NSObject {
    static let shareInstance = PAShareData()
}

extension PAShareData {
    open func changeStateToBeginEditting(view: UIView?) {
        guard let view = view else {
            return
        }
        
        for subView in view.subviews {
            if subView.isKind(of: UILabel.self) {
                let label = subView as? UILabel
                label?.textColor = .blue
            } else if subView.isMember(of: UIView.self) {
                subView.backgroundColor = .blue
            }
        }
    }
    
    open func changeStateToEndEditting(view: UIView?) {
        guard let view = view else {
            return
        }
        
        for subView in view.subviews {
            if subView.isKind(of: UILabel.self) {
                let label = subView as? UILabel
                label?.textColor = .black
            } else if subView.isMember(of: UIView.self) {
                subView.backgroundColor = .black
            }
        }
    }
    
    open func isValidEmail(email:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: email)
    }
}
