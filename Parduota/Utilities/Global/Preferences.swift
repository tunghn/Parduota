//
//  Preferences.swift
//  Parduota
//
//  Created by TungHN on 11/4/17.
//  Copyright © 2017 None. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

// MARK: Identify User Default - KEY
// Login
private let GLOBAL_USER_ACCESS_TOKEN = "global.user.access.token"
// User Data Model
private let GLOBAL_USER_DATA = "global.user.data"
// FCM token
private let GLOBAL_FCM_TOKEN = "glogal.fcm.token"

class Preferences {
    let kvoStore = UserDefaults.standard
    
    // MARK: - Login
    func isLoggedIn() -> Bool {
        guard let token = kvoStore.object(forKey: GLOBAL_USER_ACCESS_TOKEN) as? String,
            token.characters.count > 0 else {
                removeUserAccessToken()
                return false
        }
        return true
    }
    
    func updateUserAccessToken(token: String?) {
        // Log.debug("User Access Token: \(String(describing: token))")
        guard let token = token, token.characters.count > 0 else {
            removeUserAccessToken()
            return
        }
        kvoStore.set(token, forKey: GLOBAL_USER_ACCESS_TOKEN)
        kvoStore.synchronize()
    }
    
    func getUserAccessToken() -> String {
        guard let token = kvoStore.object(forKey: GLOBAL_USER_ACCESS_TOKEN) as? String else {
            return ""
        }
        return token
    }
    
    func removeUserAccessToken() {
        kvoStore.removeObject(forKey: GLOBAL_USER_ACCESS_TOKEN)
        kvoStore.synchronize()
    }
    
    // MARK: - FCM token
    func updateFCMToken(token: String?) {
        // Log.debug("User Access Token: \(String(describing: token))")
        guard let token = token, token.characters.count > 0 else {
            removeFCMToken()
            return
        }
        kvoStore.set(token, forKey: GLOBAL_FCM_TOKEN)
        kvoStore.synchronize()
    }
    
    func getFCMToken() -> String {
        guard let token = kvoStore.object(forKey: GLOBAL_FCM_TOKEN) as? String else {
            return ""
        }
        return token
    }
    
    func removeFCMToken() {
        kvoStore.removeObject(forKey: GLOBAL_FCM_TOKEN)
        kvoStore.synchronize()
    }
    
    // MARK: - User Data Model
    func updateUserDataJSON(user: PAUserModel?) {
        guard let userData = user else { return }
        
        do {
            let data = try JSONSerialization.data(withJSONObject: userData.toJSON(), options: JSONSerialization.WritingOptions(rawValue : 0))
            kvoStore.set(data, forKey: GLOBAL_USER_DATA)
            kvoStore.synchronize()
        }catch{
            Log.error("Error while serializing User Data to Preferences, error: \(error)")
        }
    }
    
    func getUser() -> PAUserModel {
        guard let anyData = kvoStore.value(forKey: GLOBAL_USER_DATA) else {
            return PAUserModel()
        }

        if let data = anyData as? Data {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String : Any]
                if let user = Mapper<PAUserModel>().map(JSONObject: json) {
                    return user
                }
            }catch{
                Log.error("Error while serializing User Data to Preferences, error: \(error)")
            }
        }
        
        return PAUserModel()
    }
}
