//
//  AppDelegate.swift
//  Parduota
//
//  Created by TungHN on 9/28/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import IQKeyboardManagerSwift
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    var notificationOrder = Notification.Name("order")

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        // Subscribe app loading
        _ = GlobalContext.appLoadOb.subscribe(onNext: { [weak self] _ in
            var isLoggedIn = false
            if let prefs = GlobalContext.get(key: GlobalContext.PREFS) {
                isLoggedIn = prefs.isLoggedIn()
            }
            
            if isLoggedIn {
                // NEED UPDATE: Change VC to Main VC (after created)
                self?.homeVC()
            } else {
                self?.loginVC()
            }
        })
        
        FirebaseApp.configure()
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        // Setup Keyboard Manager
        IQKeyboardManager.sharedManager().enable = true
        
        // Setup Facebook SDK
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //MARK: - Action
    func homeVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let itemsViewController = storyboard.instantiateViewController(withIdentifier: "PAItemsViewController") as! PAItemsViewController
        let menuViewController = R.storyboard.main.pAMenuViewController()
        let nvc: UINavigationController = UINavigationController(rootViewController: itemsViewController)
        UINavigationBar.appearance().tintColor = UIColor.blue
        menuViewController?.itemsViewController = nvc
        let slideMenuController = PASlideMenuViewController(mainViewController: nvc, leftMenuViewController: menuViewController!)
        slideMenuController.automaticallyAdjustsScrollViewInsets = true
        slideMenuController.delegate = itemsViewController
        self.window?.rootViewController = slideMenuController
        self.window?.makeKeyAndVisible()
    }
    
    func loginVC() {
        if let loginViewController = R.storyboard.login.loginScreen() {
            let nvc: UINavigationController = UINavigationController(rootViewController: loginViewController)
            self.window?.rootViewController = nvc
            self.window?.makeKeyAndVisible()
        }
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
    }

}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        pushNotification(withData: userInfo)
        
        // Change this to your preferred presentation option
        completionHandler([])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler()
    }
}
// [END ios_10_message_handling]

// MARK: - Custom method
extension AppDelegate {
    func pushNotification(withData data: [AnyHashable: Any]) {
        if let typeData = data["type"] {
            if String(describing: typeData) == "18"
                || String(describing: typeData) == "19"
                || String(describing: typeData) == "20" {
                NotificationCenter.default.post(name: notificationOrder, object: nil)
            }
        }
    }
}
