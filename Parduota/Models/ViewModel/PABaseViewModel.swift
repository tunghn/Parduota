//
//  PABaseViewModel.swift
//  Parduota
//
//  Created by TungHN on 11/3/17.
//  Copyright © 2017 None. All rights reserved.
//

import Moya

public typealias PARequestSuccessClosure = (() -> Void)
public typealias PARequestSuccessWithDataClosure = ((AnyObject) -> Void)
public typealias PARequestFailureClosure = ((String?) -> Void)

public struct ResponseKey {
    static let code     = "code"
    static let data     = "data"
    static let error  = "error"
    static let message = "message"
    static let messages = "messages"
}

public func DefaultRequestFailureHandler(_ errorResponse: Response) {
    // Check statusCode and handle desired errors
    do {
        guard let errorJson = try errorResponse.mapJSON() as? Dictionary<String, Any> else {
            SVProgressHUD.showError(withStatus: "Server process error. Please try again.")
    
            return
        }
        
        var errorStr = TSConstants.EMPTY_STRING
        if let message = errorJson[ResponseKey.error] as? String {
            errorStr = message  
        } else if let json = errorJson[ResponseKey.error] as? Dictionary<String, Any> {
            if let message = json[ResponseKey.message] as? String {
                errorStr = message
            }
        } else if let message = errorJson[ResponseKey.message] as? String {
            errorStr = message 
        } else if let message = errorJson[ResponseKey.messages] as? String {
            errorStr = message
        }
        
        SVProgressHUD.showError(withStatus: errorStr)
        if errorResponse.statusCode == 401 {
            R.storyboard.login.loginScreen()?.setAsRootVCAnimated()
        }
        
    }
    catch {
        SVProgressHUD.showError(withStatus: "Network error. Please try again.")
    }
}

class PABaseViewModel {
    
}
