//
//  PAUserViewModel.swift
//  Parduota
//
//  Created by TungHN on 11/3/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAUserViewModel: PABaseViewModel {
    enum PropertiesKey: String {
        case token
        case user
    }
    
    var token: String?
    var user: PAUserModel?
}
