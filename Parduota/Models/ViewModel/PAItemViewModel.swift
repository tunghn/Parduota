//
//  PAItemViewModel.swift
//  Parduota
//
//  Created by TungHN on 11/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

enum Item_Status: Int {
    case Not_Approved   = -3
    case Cancel         = -2
    case Draft          = -1
    case Pending        = 0
    case Sold           = 1
    case Reject         = 2
    case Active         = 3
}

class PAItemViewModel: PABaseViewModel {
    enum PropertiesKey: String {
        case current_page
        case data
        case last_page
    }
    
    var current_page: Int?
    var data: [PAItemModel]?
    var last_page: Int?
}
