//
//  PAOrdersListModel.swift
//  Parduota
//
//  Created by TungHN on 11/5/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PAOrdersListModel: PABaseDataModel {
    enum PropertiesKey: String {
        case current_page
        case data
        case from
        case last_page
        case next_page_url
        case path
        case per_page
        case prev_page_url
        case to
        case total
    }
    
    // MARK: - Properties
    var currentPage: Int = 1
    var data: Array = [PAOrderModel]()
    var from: Int = 1
    var lastPage: Int = 1
    var nextPageUrl: String = TSConstants.EMPTY_STRING
    var path: String = TSConstants.EMPTY_STRING
    var perPage: Int = 1
    var prevPageUrl: String = TSConstants.EMPTY_STRING
    var to: Int = 1
    var total: Int = 1
    
    // MARK: - Mappable
    override func mapping(map: ObjectMapper.Map) {
        super.mapping(map: map)
        
        self.currentPage                <- map[PropertiesKey.current_page.rawValue]
        self.data                       <- map[PropertiesKey.data.rawValue]
        self.from                       <- map[PropertiesKey.from.rawValue]
        self.lastPage                   <- map[PropertiesKey.last_page.rawValue]
        self.nextPageUrl                <- map[PropertiesKey.next_page_url.rawValue]
        self.path                       <- map[PropertiesKey.path.rawValue]
        self.perPage                    <- map[PropertiesKey.per_page.rawValue]
        self.prevPageUrl                <- map[PropertiesKey.prev_page_url.rawValue]
        self.to                         <- map[PropertiesKey.to.rawValue]
        self.total                      <- map[PropertiesKey.total.rawValue]
    }
    
}
