//
//  PANotificationModel.swift
//  Parduota
//
//  Created by TungHN on 12/10/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PANotificationModel: PABaseDataModel {
    enum PropertiesKey: String {
        case forValue
        case type
        case meta_data
        case readed
        case created_at
        case updated_at
        case vip
        case by
        case text
        case type_desc
        case by_user
    }
    
    //MARK: - Properties
    var forValue: Int = 0
    var meta_data: AnyObject = AnyObject.self as AnyObject
    var readed: Int = 0
    var type: Int = 0
    var createdAt: String = TSConstants.EMPTY_STRING
    var updatedAt: String = TSConstants.EMPTY_STRING
    var vip: Int = 0
    var by: Int = 0
    var text: String = TSConstants.EMPTY_STRING
    var type_desc: String = TSConstants.EMPTY_STRING
    var by_user: String = TSConstants.EMPTY_STRING
    var item: PAItemModel = PAItemModel()
    var order: PAOrderModel = PAOrderModel()
    
    // MARK: - Mappable
    override func mapping(map: ObjectMapper.Map) {
        super.mapping(map: map)
        
        self.forValue                   <- map[PropertiesKey.forValue.rawValue]
        self.meta_data                  <- map[PropertiesKey.meta_data.rawValue]
        self.readed                     <- map[PropertiesKey.readed.rawValue]
        self.type                       <- map[PropertiesKey.type.rawValue]
        self.createdAt                  <- map[PropertiesKey.created_at.rawValue]
        self.updatedAt                  <- map[PropertiesKey.updated_at.rawValue]
        self.vip                        <- map[PropertiesKey.vip.rawValue]
        self.by                         <- map[PropertiesKey.by.rawValue]
        self.text                       <- map[PropertiesKey.text.rawValue]
        self.type_desc                  <- map[PropertiesKey.type_desc.rawValue]
        self.by_user                    <- map[PropertiesKey.by_user.rawValue]
    }
}
