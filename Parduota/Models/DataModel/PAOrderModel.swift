//
//  PAOrderModel.swift
//  Parduota
//
//  Created by TungHN on 11/5/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PAOrderModel: PABaseDataModel {
    enum PropertiesKey: String {
        case created_by
        case title
        case ebay_id
        case notice
        case status
        case created_at
        case updated_at
    }
    
    //MARK: - Properties
    var createdBy: Int = 1
    var title: String = TSConstants.EMPTY_STRING
    var ebayId: String = TSConstants.EMPTY_STRING
    var notice: String = TSConstants.EMPTY_STRING
    var status: String = TSConstants.EMPTY_STRING
    var createdAt: String = TSConstants.EMPTY_STRING
    var updatedAt: String = TSConstants.EMPTY_STRING
    
    // MARK: - Mappable
    override func mapping(map: ObjectMapper.Map) {
        super.mapping(map: map)
        
        self.createdBy                  <- map[PropertiesKey.created_by.rawValue]
        self.title                      <- map[PropertiesKey.title.rawValue]
        self.ebayId                     <- map[PropertiesKey.ebay_id.rawValue]
        self.notice                     <- map[PropertiesKey.notice.rawValue]
        self.status                     <- map[PropertiesKey.status.rawValue]
        self.createdAt                  <- map[PropertiesKey.created_at.rawValue]
        self.updatedAt                  <- map[PropertiesKey.updated_at.rawValue]
    }
}
