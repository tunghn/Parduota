//
//  PAItemModel.swift
//  Parduota
//
//  Created by TungHN on 11/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PAItemModel: PABaseDataModel {
    enum PropertiesKey: String {
        case title
        case user_id
        case status
        case price
        case quantity
        case country
        case description
        case location
        case created_at
        case updated_at
        case condition
        case ebay_id
//        case ebay_timestamp
        case pay_status
        case weight
        case length
        case width
        case height
//        case shipping_type
//        case shipping_service
        case shipping_service_cost
        case shipping_service_add_cost
        case sell_for_charity
        case extra_fees
        case other_shipping
        case shipping_type_custom
        case end_item
        case buyer_info
        case payment_method
        case ebay_price
        case categories
        case user
        case media
    }
    
    //MARK: - Properties
    var title: String = TSConstants.EMPTY_STRING
    var userId: Int = 0
    var status: Int = 0
    var price: String = TSConstants.EMPTY_STRING
    var quantity: String = TSConstants.EMPTY_STRING
    var country: String = TSConstants.EMPTY_STRING
    var descriptionField: String = TSConstants.EMPTY_STRING
    var location: String = TSConstants.EMPTY_STRING
    var createdAt: String = TSConstants.EMPTY_STRING
    var updatedAt: String = TSConstants.EMPTY_STRING
    var condition: Int = 0
    var ebayId: String = TSConstants.EMPTY_STRING
//    var ebay_timestamp
    var payStatus: Int = 0
    var weight: String = TSConstants.EMPTY_STRING
    var length: String = TSConstants.EMPTY_STRING
    var width: String = TSConstants.EMPTY_STRING
    var height: String = TSConstants.EMPTY_STRING
//    var shipping_type
//    var shipping_service
    var shippingServiceCost: Int = 0
    var shippingServiceAddCost: Int = 0
    var sellForCharity: Int = 0
    var extraFees: Int = 0
    var otherShipping: Int = 0
    var shippingTypeCustom: String = TSConstants.EMPTY_STRING
    var endItem: Int = 0
    var buyerInfo: String = TSConstants.EMPTY_STRING
    var paymentMethod: String = TSConstants.EMPTY_STRING
    var ebayPrice: String = TSConstants.EMPTY_STRING
    var categories: Array = [PACategoryModel]()
    var user: Array = [PAUserModel]()
    var media: Array = [PAMediaModel]()
    
    // MARK: - Mappable
    override func mapping(map: ObjectMapper.Map) {
        super.mapping(map: map)
        
        self.title                  <- map[PropertiesKey.title.rawValue]
        self.userId                 <- map[PropertiesKey.user_id.rawValue]
        self.status                 <- map[PropertiesKey.status.rawValue]
        self.price                  <- map[PropertiesKey.price.rawValue]
        self.quantity               <- map[PropertiesKey.quantity.rawValue]
        self.country                <- map[PropertiesKey.country.rawValue]
        self.descriptionField       <- map[PropertiesKey.description.rawValue]
        self.location               <- map[PropertiesKey.location.rawValue]
        self.createdAt              <- map[PropertiesKey.created_at.rawValue]
        self.updatedAt              <- map[PropertiesKey.updated_at.rawValue]
        self.condition              <- map[PropertiesKey.condition.rawValue]
        self.ebayId                 <- map[PropertiesKey.ebay_id.rawValue]
        //        self.ebay_timestamp
        self.payStatus              <- map[PropertiesKey.pay_status.rawValue]
        self.weight                 <- map[PropertiesKey.weight.rawValue]
        self.length                 <- map[PropertiesKey.length.rawValue]
        self.width                  <- map[PropertiesKey.width.rawValue]
        self.height                 <- map[PropertiesKey.height.rawValue]
        //        self.shipping_type
        //        self.shipping_service
        self.shippingServiceCost    <- map[PropertiesKey.shipping_service_cost.rawValue]
        self.shippingServiceAddCost <- map[PropertiesKey.shipping_service_add_cost.rawValue]
        self.sellForCharity         <- map[PropertiesKey.sell_for_charity.rawValue]
        self.extraFees              <- map[PropertiesKey.extra_fees.rawValue]
        self.otherShipping          <- map[PropertiesKey.other_shipping.rawValue]
        self.shippingTypeCustom     <- map[PropertiesKey.shipping_type_custom.rawValue]
        self.endItem                <- map[PropertiesKey.end_item.rawValue]
        self.buyerInfo              <- map[PropertiesKey.buyer_info.rawValue]
        self.paymentMethod          <- map[PropertiesKey.payment_method.rawValue]
        self.ebayPrice              <- map[PropertiesKey.ebay_price.rawValue]
        self.categories             <- map[PropertiesKey.categories.rawValue]
        self.user                   <- map[PropertiesKey.user.rawValue]
        self.media                  <- map[PropertiesKey.media.rawValue]
    }
    
    open func convertToDictionary() -> Dictionary<String, Any> {
        let dict: [String: Any] = [PropertiesKey.title.rawValue: self.title,
                    PropertiesKey.description.rawValue: self.descriptionField,
                    PropertiesKey.price.rawValue: self.price,
                    PropertiesKey.quantity.rawValue: self.quantity,
                    PropertiesKey.condition.rawValue: self.condition,
                    PropertiesKey.country.rawValue: self.country,
                    PropertiesKey.location.rawValue: self.location,
                    PropertiesKey.shipping_type_custom.rawValue: self.shippingTypeCustom,
                    PropertiesKey.sell_for_charity.rawValue: self.sellForCharity,
                    PropertiesKey.weight.rawValue: self.weight,
                    PropertiesKey.length.rawValue: self.length,
                    PropertiesKey.width.rawValue: self.width,
                    PropertiesKey.height.rawValue: self.height]
        
        return dict
    }
}

