//
//  PAPivotModel.swift
//  Parduota
//
//  Created by TungHN on 11/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PAPivotModel: PABaseDataModel {
    enum PropertiesKey: String {
        case item_id
        case category_id
    }
    
    // MARK: - Properties
    var itemId: Int = 0
    var categoryId: Int = 0
    
    // MARK: Mappable
    override func mapping(map: ObjectMapper.Map) {
        self.itemId                 <- map[PropertiesKey.item_id.rawValue]
        self.categoryId             <- map[PropertiesKey.category_id.rawValue]
    }
}
