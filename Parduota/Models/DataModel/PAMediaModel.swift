//
//  PAMediaModel.swift
//  Parduota
//
//  Created by TungHN on 11/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PAMediaModel: PABaseDataModel {
    enum PropertiesKey: String {
        case user_id
        case link
        case type
        case metadata
        case created_at
        case updated_at
        case pivot
    }
    
    // MARK: - Properties
    
    var userID: Int = 0
    var link: String = TSConstants.EMPTY_STRING
    var type: String = TSConstants.EMPTY_STRING
    var metadata: String = TSConstants.EMPTY_STRING
    var createdAt: String = TSConstants.EMPTY_STRING
    var updatedAt: String = TSConstants.EMPTY_STRING
    var pivot: Array = [PAPivotModel]()
    
    // MARK: - Mappable
    override func mapping(map: ObjectMapper.Map) {
        super.mapping(map: map)
        
        self.userID         <- map[PropertiesKey.user_id.rawValue]
        self.link           <- map[PropertiesKey.link.rawValue]
        self.type           <- map[PropertiesKey.type.rawValue]
        self.metadata       <- map[PropertiesKey.metadata.rawValue]
        self.createdAt      <- map[PropertiesKey.created_at.rawValue]
        self.updatedAt      <- map[PropertiesKey.updated_at.rawValue]
        self.pivot          <- map[PropertiesKey.pivot.rawValue]
    }
}
