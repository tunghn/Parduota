//
//  PAUserModel.swift
//  Parduota
//
//  Created by TungHN on 11/4/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PAUserModel: PABaseDataModel {
    enum PropertiesKey: String {
        case uuid
        case email
        case verified
        case email_token
        case provider
        case provider_id
        case address
        case gender
        case profile_url
        case company
        case full_name
        case phone
        case viber
        case description
        case other
        case parent_user
        case vip
        case block
        case credit
        case bank_account
        case token_FCM
        case created_at
        case updated_at
        case ebay_approved
        case kick_out
        case expiry_date
    }
    
    // MARK: - Properties
    var uuid: String = TSConstants.EMPTY_STRING
    var email: String = TSConstants.EMPTY_STRING
    var verified: Int = 0
    var email_token: String = TSConstants.EMPTY_STRING
    var provider: String = TSConstants.EMPTY_STRING
    var provider_id: String = TSConstants.EMPTY_STRING
    var address: String = TSConstants.EMPTY_STRING
    var gender: String = TSConstants.EMPTY_STRING
    var profile_url: String = TSConstants.EMPTY_STRING
    var company: String = TSConstants.EMPTY_STRING
    var full_name: String = TSConstants.EMPTY_STRING
    var phone: String = TSConstants.EMPTY_STRING
    var viber: String = TSConstants.EMPTY_STRING
    var descriptionField: String = TSConstants.EMPTY_STRING
    var other: String = TSConstants.EMPTY_STRING
    var parent_user: Int = 0
    var vip: Int = 0
    var block: Int = 0
    var credit: Int = 0
    var bank_account: String = TSConstants.EMPTY_STRING
    var token_FCM: String = TSConstants.EMPTY_STRING
    var created_at: String = TSConstants.EMPTY_STRING
    var updated_at: String = TSConstants.EMPTY_STRING
    var ebay_approved: Int = 0
    var kick_out: Int = 0
    var expiry_date: String = TSConstants.EMPTY_STRING
    
    // MARK: - Mappable
    override func mapping(map: ObjectMapper.Map) {
        super.mapping(map: map)
        
        self.uuid               <- map[PropertiesKey.uuid.rawValue]
        self.email              <- map[PropertiesKey.email.rawValue]
        self.verified           <- map[PropertiesKey.verified.rawValue]
        self.email_token        <- map[PropertiesKey.email_token.rawValue]
        self.provider           <- map[PropertiesKey.provider.rawValue]
        self.provider_id        <- map[PropertiesKey.provider_id.rawValue]
        self.address            <- map[PropertiesKey.address.rawValue]
        self.gender             <- map[PropertiesKey.gender.rawValue]
        self.profile_url        <- map[PropertiesKey.profile_url.rawValue]
        self.company            <- map[PropertiesKey.company.rawValue]
        self.full_name          <- map[PropertiesKey.full_name.rawValue]
        self.phone              <- map[PropertiesKey.phone.rawValue]
        self.viber              <- map[PropertiesKey.viber.rawValue]
        self.descriptionField   <- map[PropertiesKey.description.rawValue]
        self.other              <- map[PropertiesKey.other.rawValue]
        self.parent_user        <- map[PropertiesKey.parent_user.rawValue]
        self.vip                <- map[PropertiesKey.vip.rawValue]
        self.block              <- map[PropertiesKey.block.rawValue]
        self.credit             <- map[PropertiesKey.credit.rawValue]
        self.bank_account       <- map[PropertiesKey.bank_account.rawValue]
        self.token_FCM          <- map[PropertiesKey.token_FCM.rawValue]
        self.created_at         <- map[PropertiesKey.created_at.rawValue]
        self.updated_at         <- map[PropertiesKey.updated_at.rawValue]
        self.ebay_approved      <- map[PropertiesKey.ebay_approved.rawValue]
        self.kick_out           <- map[PropertiesKey.kick_out.rawValue]
        self.expiry_date        <- map[PropertiesKey.expiry_date.rawValue]
    }
    
    open func convertToDictionary() -> Dictionary<String, Any> {
        let dict: [String: Any] = [PropertiesKey.bank_account.rawValue: self.bank_account,
                                   PropertiesKey.gender.rawValue: self.gender,
                                   PropertiesKey.full_name.rawValue: self.full_name,
                                   PropertiesKey.company.rawValue: self.company,
                                   PropertiesKey.address.rawValue: self.address,
                                   PropertiesKey.phone.rawValue: self.phone,
                                   PropertiesKey.description.rawValue: self.descriptionField,
                                   PropertiesKey.other.rawValue: self.other]
        
        return dict
    }
}
