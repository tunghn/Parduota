//
//  PACategoryModel.swift
//  Parduota
//
//  Created by TungHN on 11/8/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PACategoryModel: PABaseDataModel {
    enum PropertiesKey: String {
        case CategoryID
        case CategoryParentID
        case CategoryLevel
        case CategoryName
        case Count
        case created_at
        case updated_at
        case pivot
    }
    
    // MARK: - Properties
    var categoryID: Int = 0
    var categoryParentID: Int = 0
    var categoryLevel: Int = 0
    var categoryName: String = TSConstants.EMPTY_STRING
    var count: Int = 0
    var createdAt: String = TSConstants.EMPTY_STRING
    var updatedAt: String = TSConstants.EMPTY_STRING
    var pivot: PAPivotModel = PAPivotModel()
    
    // MARK: - Mappable
    override func mapping(map: ObjectMapper.Map) {
        super.mapping(map: map)
        
        self.categoryID                 <- map[PropertiesKey.CategoryID.rawValue]
        self.categoryParentID           <- map[PropertiesKey.CategoryParentID.rawValue]
        self.categoryLevel              <- map[PropertiesKey.CategoryLevel.rawValue]
        self.categoryName               <- map[PropertiesKey.CategoryName.rawValue]
        self.count                      <- map[PropertiesKey.Count.rawValue]
        self.createdAt                  <- map[PropertiesKey.created_at.rawValue]
        self.updatedAt                  <- map[PropertiesKey.updated_at.rawValue]
        self.pivot                      <- map[PropertiesKey.pivot.rawValue]
    }
}
