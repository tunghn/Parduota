//
//  PAMessageModel.swift
//  Parduota
//
//  Created by TungHN on 12/10/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import ObjectMapper

class PAMessageModel: PABaseDataModel {
    enum PropertiesKey: String {
        case messages
        case order_id
        case created_by
        case type
        case created_at
        case updated_at
    }
    
    //MARK: - Properties
    var messages: String = TSConstants.EMPTY_STRING
    var order_id: String = TSConstants.EMPTY_STRING
    var created_by: String = TSConstants.EMPTY_STRING
    var type: String = TSConstants.EMPTY_STRING
    var createdAt: String = TSConstants.EMPTY_STRING
    var updatedAt: String = TSConstants.EMPTY_STRING
    
    // MARK: - Mappable
    override func mapping(map: ObjectMapper.Map) {
        super.mapping(map: map)
        
        self.messages                   <- map[PropertiesKey.messages.rawValue]
        self.order_id                   <- map[PropertiesKey.order_id.rawValue]
        self.created_by                 <- map[PropertiesKey.created_by.rawValue]
        self.type                       <- map[PropertiesKey.type.rawValue]
        self.createdAt                  <- map[PropertiesKey.created_at.rawValue]
        self.updatedAt                  <- map[PropertiesKey.updated_at.rawValue]
    }
}
