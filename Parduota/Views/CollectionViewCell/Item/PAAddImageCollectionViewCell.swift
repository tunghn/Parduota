//
//  PAAddImageCollectionViewCell.swift
//  Parduota
//
//  Created by TungHN on 11/27/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAAddImageCollectionViewCell: TSBaseCollectionViewCell {
    
    @IBOutlet weak var addNewImage: UIImageView!
    
    override func awakeFromNib() {
        
    }
    
    override func configCellWithData(data: Any?) {
        
    }
}
