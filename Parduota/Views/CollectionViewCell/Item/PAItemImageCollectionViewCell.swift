//
//  PAItemImageCollectionViewCell.swift
//  Parduota
//
//  Created by TungHN on 11/27/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

protocol PAItemImageCollectionViewCellDelegate:class {
    func deleteImageInIndex(index: IndexPath)
}

class PAItemImageCollectionViewCell: TSBaseCollectionViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var deleteImageButton: UIButton!
    
    override func awakeFromNib() {
        _ = deleteImageButton.then {
            _ = $0.rx.controlEvent(.touchUpInside)
                .throttle(PAAppConstants.THROTTLE_VALUE, scheduler: MainScheduler.instance)
                .takeUntil($0.rx.deallocated)
                .subscribe(onNext: { [weak self] in
                    guard let indexPath = self?.indexPath else { return }
                    (self?.delegate as? PAItemImageCollectionViewCellDelegate)?.deleteImageInIndex(index: indexPath)
                })
        }
    }
    
    override func configCellWithData(data: Any?) {
        if let media = data as? PAMediaModel {
            self.itemImage.sd_setImage(with: URL.init(string: PAAppConstants.URL_BASE_IMAGE + media.link), placeholderImage: R.image.no_image())
        }
    }
    

    
}
