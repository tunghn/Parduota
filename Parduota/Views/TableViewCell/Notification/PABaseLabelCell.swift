//
//  PABaseLabelCell.swift
//  Parduota
//
//  Created by TungHN on 12/25/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PABaseLabelCell: TSBaseTableViewCell {

    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
