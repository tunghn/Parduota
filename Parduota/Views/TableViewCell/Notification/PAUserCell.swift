//
//  PAUserCell.swift
//  Parduota
//
//  Created by TungHN on 12/25/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAUserCell: TSBaseTableViewCell {

    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        containView.layer.cornerRadius = valueLabel.frame.size.height / 2
        containView.layer.borderWidth = 0.5
        containView.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
