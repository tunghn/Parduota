//
//  PANotificationCell.swift
//  Parduota
//
//  Created by TungHN on 12/11/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PANotificationCell: TSBaseTableViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func configCellWithData(data: Any?) {
        if let notification = data as? PANotificationModel {
            createdLabel.text = notification.createdAt
            if notification.readed == 0 {
                iconImageView.image = R.image.icon_close_mail()
                self.backgroundColor = UIColor.init(white: 0.8, alpha: 1)
            } else {
                iconImageView.image = R.image.icon_open_mail()
                self.backgroundColor = .white
            }
            
            switch notification.type {
            case 0:
                titleLabel.text = R.string.localizable.created_new_item(notification.item.title)
                break
            case 1:
                titleLabel.text = R.string.localizable.updated_item(notification.item.title)
                break
            case 2:
                titleLabel.text = R.string.localizable.updatedIs_sold(notification.item.title)
                break
            case 3:
                titleLabel.text = R.string.localizable.send_a_request_vip()
                break
            case 4:
                titleLabel.text = R.string.localizable.you_have_been_unlocked()
                break
            case 5:
                titleLabel.text = R.string.localizable.you_are_blocked()
                break
            case 6:
                titleLabel.text = R.string.localizable.updatedIs_reject(notification.item.title)
                break
            case 7:
                titleLabel.text = R.string.localizable.delete_item(notification.item.title)
                break
            case 8:
                titleLabel.text = R.string.localizable.your_account_is_upgraded_to_VIP()
                break
            case 9:
                titleLabel.text = R.string.localizable.your_account_is_upgraded()
                break
            case 10:
                titleLabel.text = R.string.localizable.your_account_is_downgraded()
                break
            case 11:
                titleLabel.text = R.string.localizable.updatedIs_cancel(notification.item.title)
                break
            case 12:
                titleLabel.text = R.string.localizable.item_with_nameIs_approved(notification.item.title)
                break
            case 13:
                titleLabel.text = R.string.localizable.item_with_nameIs_active_on_ebay(notification.item.title)
                break
            case 14:
                titleLabel.text = R.string.localizable.make_new_for_you_an_offer_abouts_the_price(notification.item.title)
                break
            case 15:
                titleLabel.text = R.string.localizable.make_new_for_you_an_offer_abouts_the_price(notification.item.title)
                break
            case 16:
                titleLabel.text = R.string.localizable.make_for_you_decline_item_click_to_view(notification.item.title)
                break
            case 17:
                titleLabel.text = R.string.localizable.accept_offer_item_click_to_view(notification.item.title)
                break
            case 18:
                titleLabel.text = R.string.localizable.created_new_order_ebay(notification.order.title)
                break
            case 19:
                titleLabel.text = R.string.localizable.have_a_new_message_on_order()
                break
            case 20:
                titleLabel.text = R.string.localizable.close_an_order(notification.order.title)
                break
            case 21:
                titleLabel.text = R.string.localizable.you_need_buy_credit_for_service_charge()
                break
            case 22:
                titleLabel.text = R.string.localizable.service_free()
                break
            case 23:
                titleLabel.text = R.string.localizable.all_item_is_cancel()
                break
            default:
                break
            }
        }
    }
}
