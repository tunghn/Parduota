//
//  PAItemTableViewCell.swift
//  Parduota
//
//  Created by TungHN on 11/11/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit
import SDWebImage

class PAItemTableViewCell: TSBaseTableViewCell {

    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func configCellWithData(data: Any?) {
        if let item = data as? PAItemModel {
            let media = item.media.first
            if let imageURL = media?.link {
                itemImage.sd_setImage(with: URL.init(string: PAAppConstants.URL_BASE_IMAGE + imageURL), placeholderImage: R.image.no_image())
            } else {
                itemImage.image = R.image.no_image()
            }
            
            titleLabel.text = item.title
            descriptionLabel.text = item.descriptionField
            quantityLabel.text = String(item.quantity)
            priceLabel.text = String(item.price)
            createdLabel.text = item.createdAt
        }
    }

}
