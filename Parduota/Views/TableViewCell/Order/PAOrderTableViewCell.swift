//
//  PAOrderTableViewCell.swift
//  Parduota
//
//  Created by TungHN on 11/6/17.
//  Copyright © 2017 None. All rights reserved.
//

import UIKit

class PAOrderTableViewCell: TSBaseTableViewCell {

    @IBOutlet weak var titleOrderLabel: UILabel!
    @IBOutlet weak var statusOrderLabel: UILabel!
    @IBOutlet weak var ebayIdLabel: UILabel!
    @IBOutlet weak var createdLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func configCellWithData(data: Any?) {
        guard let order = data as? PAOrderModel else {
            return
        }
        
        titleOrderLabel.text = order.title
        ebayIdLabel.text = order.ebayId
        statusOrderLabel.text = order.status
        createdLabel.text = order.createdAt
    }

}
