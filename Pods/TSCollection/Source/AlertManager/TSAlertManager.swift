//
//  TSAlertManager.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation
import SDCAlertView

/** TSAlertManager Struct

*/
public struct TSAlertManager {

}

public extension TSAlertManager {

    // MARK: - Alert

    static func showAlert(title: String? = nil, message: String? = nil, actions: AlertAction...) {
        let alert = AlertController(title: title, message: message)

        actions.forEach {
            alert.add($0)
        }

        alert.present()
    }

    static func showAlertWithAnAction(title: String? = nil, message: String? = nil, actionTitle: String) {
        AlertController.alert(withTitle: title, message: message, actionTitle: actionTitle)
    }
    
    static func showAlertTextField(title: String? = nil, message: String? = nil, actions: AlertAction..., textFieldAction: @escaping (UITextField) -> Void) {
        let alert = AlertController(title: title, message: message)
        
        
        alert.addTextField { (textField) in
            textFieldAction(textField)
        }
        
        actions.forEach {
            alert.add($0)
        }
        
        alert.present()
    }
}
