//
//  String+TSAttributeString.swift
//  TSCollection
//
//  Created by Jimmy on 11/8/16.
//  Copyright © 2016 TS. All rights reserved.
//

import UIKit

public extension String {
    
    
}

public extension String {
    public static func getAttributeStringWithString(str: NSString, configs: [String: AnyObject]) -> NSMutableAttributedString {
        let attributesString = NSMutableAttributedString(string: str as String)
        
        for attr in configs {
            let range = str.range(of: attr.0)
            if let config = attr.1 as? [String: AnyObject] {
                attributesString.addAttributes(config, range: range)
            }
        }
        
        return attributesString
    }
}

