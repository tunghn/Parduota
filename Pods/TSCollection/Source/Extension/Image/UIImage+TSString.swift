//
//  UIImage+TSString.swift
//  TSCollection
//
//  Created by sanghv on 10/24/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSString Extends UIImage

*/
extension UIImage {
    
    open class func image(fromText text: String, font: UIFont, color: UIColor, maxSize: CGSize) -> UIImage {
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineBreakMode = NSLineBreakMode.byWordWrapping
        paragraph.alignment = .center // potentially this can be an input param too, but i guess in most use cases we want center align

        let attributedString = NSAttributedString(string: text, attributes:
            [NSFontAttributeName: font,
             NSForegroundColorAttributeName: color,
             NSParagraphStyleAttributeName: paragraph]
        )

        let size = attributedString.boundingRect(with: maxSize, options: .usesLineFragmentOrigin, context: nil).size
        
        UIGraphicsBeginImageContextWithOptions(size, false , 0.0)
        attributedString.draw(in: CGRect(origin: .zero, size: size))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image!
    }
}
