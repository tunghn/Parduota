//
//  UINavigationController+TSRotationExtension.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSRotationExtension Extends UINavigationController

*/
extension UINavigationController {
    
    // MARK: - Rotation

    override open var shouldAutorotate: Bool {
        get {
            return self.visibleViewController?.shouldAutorotate ?? true
        }
    }

    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return self.visibleViewController?.supportedInterfaceOrientations ?? .all
        }
    }

    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            return self.visibleViewController?.preferredInterfaceOrientationForPresentation ?? .unknown
        }
    }
}

extension UIAlertController {
    open override var shouldAutorotate: Bool {
        return true
    }

    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }

    open override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .unknown
    }
}
