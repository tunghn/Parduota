//
//  UIViewController+TSKeyboardExtension.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSKeyboardExtension Extends UIViewController

*/
extension UIViewController {
    
    open func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillShow(notifcation:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(UIViewController.keyboardWillHide(notifcation:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    open func destroyForKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    open func keyboardRectFromNotification(notifcation: NSNotification) -> CGRect {
        let keyboardRect = (notifcation.userInfo?[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue

        return keyboardRect
    }

    open func keyboardWillShow(notifcation: NSNotification) {

    }

    open func keyboardWillHide(notifcation: NSNotification) {
        
    }
}
