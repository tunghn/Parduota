//
//  TSSessionDelegate.swift
//  TSCollection
//
//  Created by sanghv on 10/18/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Alamofire

public typealias SessionCompletionHandlerClosure = (() -> Void)

open class TSSessionDelegate: SessionDelegate {

    fileprivate var completionHandlerDictionary = [String: SessionCompletionHandlerClosure]()
}

extension TSSessionDelegate {

    override open func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        guard let identifier = session.configuration.identifier else {
            return
        }

        self.callCompletionHandler(forSessionIdentifier: identifier)
    }
}

extension TSSessionDelegate {

    open func addCompletion(handler: @escaping SessionCompletionHandlerClosure, identifier: String) {
        self.completionHandlerDictionary[identifier] = handler
    }

    fileprivate func callCompletionHandler(forSessionIdentifier identifier: String) {
        guard let handler = self.completionHandlerDictionary[identifier] else {
            return
        }

        handler()
    }
}
