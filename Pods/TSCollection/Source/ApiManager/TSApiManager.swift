//
//  TSApiManager.swift
//  TSCollection
//
//  Created by sanghv on 10/18/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation
import Alamofire
import Moya
import MoyaSugar
import RxSwift
import SwiftyJSON

/** TSApiManager Class

*/

public typealias TSJSONClosure                 = (JSON) -> ()
public typealias TSErrorClosure                = (Moya.Response) -> ()
public typealias TSDownloadProgressClosure     = Request.ProgressHandler
public typealias TSDownloadCompletedClosure    = (NSURL?, Moya.MoyaError?) -> ()

public struct TSResponseKey {
    static let message  = "message"
}

open class TSApiManager {

    open static let shared = TSApiManager()

    private init() {

    }

    open var networkAvailable: Bool = true {
        didSet {

        }
    }

    open var isNetworkAvailable: Bool {
        get {
            return self.networkAvailable
        }
    }

    lazy fileprivate var networkReachabilityManager: NetworkReachabilityManager? = {
        let networkReachabilityManager = NetworkReachabilityManager()

        networkReachabilityManager?.listener = { [unowned self] status in
            self.networkAvailable = (status == .reachable(.ethernetOrWiFi) || status == .reachable(.wwan))
        }

        return networkReachabilityManager
    }()

    private static let BACKGROUND_CONFIGURATION_IDENTIFIER: String = (Bundle.main.infoDictionary?["CFBundleIdentifier"] as! String) + ".background"

    open var sessionDelegate = TSSessionDelegate()

    let disposeBag = DisposeBag()

    open var timeoutIntervalForRequest: TimeInterval = 15

    open var timeoutIntervalForResource: TimeInterval = 60

    open lazy var timeoutIntervalForLongRequest: TimeInterval = {
        return self.timeoutIntervalForRequest * 40
    }()

    open lazy var timeoutIntervalForLargeResource: TimeInterval = {
        return self.timeoutIntervalForResource * 10
    }()

    open lazy var manager: Manager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Manager.defaultHTTPHeaders

        configuration.timeoutIntervalForRequest = self.timeoutIntervalForRequest
        configuration.timeoutIntervalForResource = self.timeoutIntervalForResource

        let manager = Manager(configuration: configuration)

        return manager
    }()

    open lazy var managerForLongTask: Manager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Manager.defaultHTTPHeaders

        configuration.timeoutIntervalForRequest = self.timeoutIntervalForLongRequest
        configuration.timeoutIntervalForResource = self.timeoutIntervalForLargeResource

        let managerForLongTask = Manager(configuration: configuration)

        return managerForLongTask
    }()

    open lazy var backgroundManager: Manager = {
        let configuration = URLSessionConfiguration.background(withIdentifier: TSApiManager.BACKGROUND_CONFIGURATION_IDENTIFIER)
        configuration.httpAdditionalHeaders = Manager.defaultHTTPHeaders

        let backgroundManager = Manager(configuration: configuration, delegate: self.sessionDelegate)

        return backgroundManager
    }()
}

extension TSApiManager {

    // MARK: - Network Reachability

    fileprivate func startNetworkReachabilityListening() {
        self.networkReachabilityManager?.startListening()
    }

    fileprivate func stopNetworkReachabilityListening() {
        self.networkReachabilityManager?.listener = nil
        self.networkReachabilityManager?.stopListening()
    }
}

extension TSApiManager {

    // MARK: - Create provider

    open func createProviderFor<API: SugarTargetType>(_: API.Type, manager: Manager = TSApiManager.shared.manager) -> RxMoyaSugarProvider<API> {
        let provider = RxMoyaSugarProvider<API>(manager: manager)

        /*
        let provider = RxMoyaSugarProvider<API>(endpointClosure: { (target: API) -> Endpoint<API> in

            let endpointClosure = MoyaProvider<API>.DefaultEndpointMapping
            let defaultEndpoint = endpointClosure(target)

            let endpoint = Endpoint<API>(
                URL: defaultEndpoint.URL,
                sampleResponseClosure: {.networkResponse(200, target.sampleData)},
                method: defaultEndpoint.method,
                parameters: defaultEndpoint.parameters,
                parameterEncoding: target.params?.encoding ?? defaultEndpoint.parameterEncoding,
                httpHeaderFields: target.httpHeaderFields ?? defaultEndpoint.httpHeaderFields
            )

            return endpoint
        }, manager: manager)
         */

        return provider
    }
    
    private func url(target: TargetType) -> String {
        return target.baseURL.appendingPathComponent(target.path).absoluteString
    }
}

extension TSApiManager {

    // MARK: - Request

    open func request<API: SugarTargetType>(api: API, provider: RxMoyaSugarProvider<API>? = nil, longTask: (Bool) = false, nextHandler: TSJSONClosure? = nil, errorHandle: TSErrorClosure? = nil) {

        let managerRef = longTask ? managerForLongTask : manager
        let provider = provider ?? self.createProviderFor(API.self, manager: managerRef)

        provider.request(api)
            .filterSuccessfulStatusCodes()
            .subscribe(errorClosure: { (errorResponse) in
                errorHandle?(errorResponse)
            })
            .mapJSON()
            .subscribe(onNext: { (element) in
                nextHandler?(JSON(element))
            })
            .addDisposableTo(self.disposeBag)
    }
}

extension Observable {

    // MARK: - Error handle

    fileprivate func subscribe(errorClosure: (TSErrorClosure)?) -> Observable<Element> {
        return self.do(onError: { (e) in
            // Unwrap underlying error

            guard let error = e as? Moya.MoyaError else {
                let response = Moya.Response(statusCode: 0, data: Data())
                errorClosure?(response)

                throw e
            }

            guard case .statusCode(let response) = error else {

                guard case .underlying(let swiftError) = error, let objcError = swiftError as? NSError else {
                    let response = Moya.Response(statusCode: 0, data: Data())
                    errorClosure?(response)

                    throw e
                }

                do {
                    let data = try JSON(dictionaryLiteral: (TSResponseKey.message, objcError.localizedDescription)).rawData()
                    let systemResponse = Moya.Response(statusCode: objcError.code, data: data)

                    errorClosure?(systemResponse)

                    return
                }
                catch (let exception) {
                    let response = Moya.Response(statusCode: 0, data: Data())
                    errorClosure?(response)

                    throw exception
                }
            }

            // Callback error response

            errorClosure?(response)
        })
    }
}

extension TSApiManager {

    // MARK: - Download

    open func download(urlString: String?, progressHandler: TSDownloadProgressClosure?, completionHandler: TSDownloadCompletedClosure?) {
        guard let urlStringTemp = urlString, let url = NSURL(string: urlStringTemp) as URL? else {

            return
        }

        let lastPathComponent = url.lastPathComponent
        TSApiManager.removeFile(name: lastPathComponent, inDirectory: TSApiManager.applicationDirectory()!)

        let urlRequest = URLRequest(url: url)
        let downloadRequest = backgroundManager.download(urlRequest, to: DownloadRequest.suggestedDownloadDestination())

        downloadRequest.downloadProgress { (progress) in
            progressHandler?(progress)
        }

        downloadRequest.response { (downloadResponse) in
            if let error = downloadResponse.error as? Moya.MoyaError {
                completionHandler?(nil, error)

                return
            }

            guard let url = downloadResponse.request?.url, let response = downloadResponse.response else {
                completionHandler?(nil, nil)

                return
            }

            let downloadDestination = DownloadRequest.suggestedDownloadDestination()
            let destinationURL = downloadDestination(url, response).destinationURL as NSURL?

            completionHandler?(destinationURL, nil)
        }
    }
}

extension TSApiManager {

    // MARK: - Document directory

    open class func applicationDirectory (directory: FileManager.SearchPathDirectory = .documentDirectory, domain: FileManager.SearchPathDomainMask = .userDomainMask) -> NSURL? {
        let directoryURLs = FileManager.default.urls(for: directory, in: domain)

        guard !directoryURLs.isEmpty else {
            return nil
        }

        return directoryURLs[0] as NSURL?
    }

    // MARK: - Remove file

    open class func removeFile(name: String, inDirectory directory: NSURL) {
        let filePathURL = directory.appendingPathComponent(name)

        if let filePathURL = filePathURL, FileManager.default.fileExists(atPath: filePathURL.path) == true {
            try? FileManager.default.removeItem(at: filePathURL)
        }
    }
}
