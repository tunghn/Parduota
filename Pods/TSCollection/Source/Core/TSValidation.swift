//
//  TSValidation.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSValidation Struct

*/
public struct TSValidation {

}

extension TSValidation {

    // Only allow numbers. Look for anything not a number.
    public static func isStringNumerical(string : String) -> Bool {
        let range = string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted)

        return (range == nil)
    }

    // Only allow numbers. Look for anything not a number.
    public static func isStringInRange(string : String, min : Int, max : Int) -> Bool {
        return (string.characters.count >= min && string.characters.count <= max)
    }

    public static func isAlphaNumeric(string : String) -> Bool {
        let matches =  self.matchesForRegexInText(regex: "^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$", text: string)
        return matches.count != 0;
    }

    public static func isNumeric(string : String) -> Bool {
        let numberFromString = Int(string)
        return numberFromString != nil
    }

    public static func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }

    public static func matchesForRegexInText(regex: String!, text: String!) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matches(in: text,
                                        options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
}

public extension TSValidation {

    // MARK: - Device info

    static var isPad: Bool {
        get {
            switch UI_USER_INTERFACE_IDIOM() {
            case .pad:
                return true

            default:
                return false

            }
        }
    }
}
