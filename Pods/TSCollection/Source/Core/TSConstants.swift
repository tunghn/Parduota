//
//  TSConstants.swift
//  TSCollection
//
//  Created by sanghv on 10/18/16.
//  Copyright © 2016 TS. All rights reserved.
//

import Foundation

/** TSConstants Struct

*/
public struct TSConstants {

}

public extension TSConstants {

    // MARK: - String constant

    static let kPLIST_TYPE              = "plist"
    static let kJSON_TYPE               = "json"
    static let kPNG_TYPE                = "png"
    static let kJPG_TYPE                = "jpg"
    static let EMPTY_STRING             = ""
    static let WHITESPACE_STRING        = " "
    static let DOT_STRING               = "."
    static let COMMA_STRING             = ","
    static let SLASH_STRING             = "/"
    static let BACK_SLASH_STRING        = "\\"
    static let EMPTY_PERCENT_STRING     = "0%"
    static let PERCENT_STRING           = "%"
    static let NEW_LINE_STRING          = "\n"
    static let ZERO_STRING              = "0"
    static let ZERO_DATE_TIME           = "0000-00-00 00:00:00"

    // MARK: - Scheme

    static let WIFI_SCHEME              = "prefs:root=WIFI"
}

public extension TSConstants {

    // MARK: - API

    static let SERVER_ERROR_STATUS_CODE: Int        = 500
    static let SERVER_ERROR_UNAUTHORIZED_STATUS_CODE: Int   = 401
    static let SERVER_ERROR_NOTFOUND_STATUS_CODE: Int       = 400
    static let SERVER_RESPONSE_STATUS_CODE: Int     = 200
}
