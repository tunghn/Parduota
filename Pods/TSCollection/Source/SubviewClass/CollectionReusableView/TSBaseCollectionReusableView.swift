//
//  TSBaseCollectionReusableView.swift
//  TSCollection
//
//  Created by sanghv on 10/19/16.
//  Copyright © 2016 TS. All rights reserved.
//

import UIKit

open class TSBaseCollectionReusableView: UICollectionReusableView {
        
    deinit {

    }

    open weak var delegate: AnyObject?
    open var section: Int?

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.configView()
    }

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override open func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        self.configView()
    }
}

extension TSBaseCollectionReusableView {

    open func configView() {

    }

    open func setSection(section: Int?, sender: AnyObject?) {
        self.section = section
        self.delegate = sender
    }
}

public extension TSBaseCollectionReusableView {

    // MARK: - Reuse identifer

    class var identifier: String {
        get {
            let mirror = Mirror(reflecting: self)
            return "\(String(describing: mirror.subjectType).replacingOccurrences(of: ".Type", with: ""))ID"
        }
    }
}

extension TSBaseCollectionReusableView: TSHeaderDatasource {

    // MARK: - TSHeaderDatasource

    public class var headerIdentifier: String {
        get {
            return "HeaderIdentifier"
        }
    }

    public func configHeaderWithData(data: Any?) {

    }
}
